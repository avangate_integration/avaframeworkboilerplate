avaDebug = true;

/**
 * Global
 */
AvaCart.Callbacks.global = {
	callback: function () {
		AvaCart.PageStructure.initialize();

		/* ==========================================================================
	   SELECT PAGE LAYOUT
	   ========================================================================== */
		//        AvaCart.Layouts.layout1.initialize();
		//		 AvaCart.Layouts.layout2.initialize();
		//		 AvaCart.Layouts.layout2.initialize();
		//         AvaCart.Layouts.layout3.initialize();
		//		AvaCart.Layouts.layout4.initialize();
		AvaCart.Layouts.layout7.initialize();

		AvaCart.Widgets.selectBoxes.before = function () {
			$('#payment option[value="1"]').html(function (i, text) {
				return text.replace('/Eurocard', '');
			});
		};
		AvaCart.Widgets.selectBoxes.initialize();
		AvaCart.Widgets.validateMandatoryFields.initialize();
		AvaCart.Widgets.hotline.initialize({
			placeholder: '.top-info',
			HOTLINE: avaHotline.HOTLINE,
			//			SUPPORT: avaHotline.SUPPORT,
			phone: avaHotline.phones[0],
			showLanguageInHeader: avaPage.isCheckoutPage // display language only on Checkout Page
				//            showLanguageInHeader: false
		});
		AvaCart.Widgets.logo.initialize({
			placeholder: '.site-branding',
			link: omniture_vars ? omniture_vars.VENDOR_URL : null,
			image: omniture_vars ? omniture_vars.VENDOR_LOGO : null
		});
        
        AvaCart.Widgets.inputWrappers.initialize();
        
        AvaCart.Widgets.hideFields.initialize();
	}
};

/**
 * Checkout Graceful Degradation
 */
AvaCart.Callbacks.checkoutGracefulDegradation = {
	condition: (avaPage.current === 'checkout') && (!((avaPage.browser === 'ie-7') || (avaPage.browser === 'ie-8') || (avaPage.browser === 'ie-9'))),
	callback: function () {
		AvaCart.Widgets.creditCard.before = function () {
			$('#credit__cart__fields__container').append('<tr class="credit-card-container"><td class="credit-card-label"></td><td class="credit-card-wrapper"></td></tr>');
            $('.credit-card-container').hide();
            if (_t_settings && _t_settings.accepted_cards && _t_settings.accepted_cards.hasOwnProperty($('#payment').val())) {
                $('.credit-card-container').show();
            }
		};
		AvaCart.Widgets.creditCard.initialize({
			placeholder: '.credit-card-wrapper'
		});
	}
};

/**
 * Checkout Page
 */
AvaCart.Callbacks.checkoutPage = {
    condition: avaPage.current === 'checkout',
    callback: function () {
        AvaCart.Widgets.productsInShoppingCart.initialize({
            showCurrencyInHeader: true,
            showRemoveIconFirst: true,
            showProductImage: true,
            productImageSize: 80,
            hideVATtext: false,
            layout: 'layout-4' // AVAILABLE OPTIONS: [default, layout-1, layout-2, layout-3, layout-4]
        });

		//        AvaCart.Widgets.customPlaceholders.initialize({
		//            placeholderSupport: ('placeholder' in document.createElement('input'))
		//        });

//		AvaCart.Widgets.fieldHelper.initialize({
//			placeholder: '#card__data__nr .order__checkout__form__input',
//			iconClass: 'icon-info-circled',
//			contentTypeHtml: false,
//            mirrored: true,
//			contentImageSrc: 'https://secure.avangate.com/images/merchant/67caec8041b2d689a5035d3bf441c34c/security-code-front.png'
//				//            contentTypeHtml: true,
//				//            contentHtml: '<p>Lorem ipsum</p>'
//		});

		AvaCart.Widgets.fieldHelper.initialize({
			placeholder: '#card__data__cvvc .order__checkout__form__input',
			iconClass: 'icon-info-circled',
			contentTypeHtml: true,
            mirrored: false,
			contentHtml: '<img class="cvv-image-helper" alt="" src="https://secure.avangate.com/images/merchant/67caec8041b2d689a5035d3bf441c34c/security-code-back.png"/><img class="amex-cvv-image-helper" alt="" src="https://secure.avangate.com/images/merchant/67caec8041b2d689a5035d3bf441c34c/security-code-front.png"/>'
		});

//        AvaCart.Widgets.secureCheckout.settings.content = __order_processed_by.DESCRIPTION.split('.');
//        AvaCart.Widgets.secureCheckout.settings.content.shift();
//        AvaCart.Widgets.secureCheckout.settings.content = AvaCart.Widgets.secureCheckout.settings.content.join('.');
        AvaCart.Widgets.secureCheckout.settings.content = '';
        AvaCart.Widgets.secureCheckout.before = function () {
			$('.order__checkout__bottom__content').prepend('<li id="order__secure__checkout" class="col-md-6"></li>');
		};
		AvaCart.Widgets.secureCheckout.initialize({
			placeholder: '#order__secure__checkout',
            headerLabel: __order_widgets.SECURE_CHECKOUT,
            headerIconClass: 'icon-lock',
//            showBoxTitle: true,
            showBox: false,
            sealsImageSrc: 'https://secure.avangate.com/images/merchant/67caec8041b2d689a5035d3bf441c34c/secure-checkout.png',
		});

        AvaCart.Widgets.whoisAvangate.initialize({
            placeholder: '#order__secure__checkout .secure-checkout-content-text',
            orderProcessedBy: __order_processed_by,
            vendorLogoSrc: omniture_vars ? omniture_vars.VENDOR_LOGO : ''
        });

        AvaCart.Widgets.crossSelling.before = function () {
            $('#order__totals').before('<div id="order__cross__sell__contents" class="cross__sell__products__listing products"></div>');
        };
        AvaCart.Widgets.crossSelling.initialize({
            showBox: false,
            showProductImage: false,
            addToCartIconContent: '',
            addToCartIconClass: '',
//            showProductDescription: true,
            crossSellProducts: cross_sell_products,
            displayCurrency: omniture_vars.DISPLAY_CURRENCY,
            billingCurrency: omniture_vars.BILLING_CURRENCY,
            placeholder: '#order__cross__sell__contents',
            maxProductsPerRow: 3,

			headerLabel: $('.order__crossselling_title').text() || 'We also recommend',
			addToCartLabel: $('.cross__sell__delimiter input.submit-button').val() || (typeof __order_widgets !== 'undefined' ? __order_widgets.ADD_LABEL : 'Add to cart') || 'Add to cart'
        });
                                                
        AvaCart.Widgets.cartSteps.before = function () {
            $('#subheader > .container > .row').prepend('<div class="cart-steps"></div>');
        };
		AvaCart.Widgets.cartSteps.initialize({
			placeholder: '.cart-steps',

			steps: {
				1: {
					url: 'cart.php',
					label: (__order_steps.STEP_1 !== undefined ? __order_steps.STEP_1 : 'Shopping Cart'),
					page: 'cart'
				},
				2: {
					url: 'checkout.php',
					label: (__order_steps.STEP_2 !== undefined ? __order_steps.STEP_2 : 'Billing Informations'),
					page: 'checkout'
				},
				3: {
					url: 'verify.php',
					label: (__order_steps.STEP_3 !== undefined ? __order_steps.STEP_3 : 'Confirmation and Payment'),
					page: 'verify'
				},
				4: {
					url: 'finish.php',
					label: (__order_steps.STEP_4 !== undefined ? __order_steps.STEP_4 : 'Order finished'),
					page: 'finish'
				}
			},
			//                startFromStep   : 2,
			showOnPages: ['cart', 'checkout', 'verify', 'finish'],
			//                removeSteps     : ['2'],
			showLabelsOnTop: false,
			colorCheckedSteps: false,
			colorCurrentStep: false,
			barStyle: 'solid',
			//			layout: 'default' // AVAILABLE OPTIONS: [default, preset-1, preset-2, preset-3, preset-4, preset-5]
			//            layout: 'preset-1' // AVAILABLE OPTIONS: [default, preset-1, preset-2, preset-3, preset-4, preset-5]
			//            layout: 'preset-2' // AVAILABLE OPTIONS: [default, preset-1, preset-2, preset-3, preset-4, preset-5]
			//            layout: 'preset-3' // AVAILABLE OPTIONS: [default, preset-1, preset-2, preset-3, preset-4, preset-5]
			layout: 'preset-4' // AVAILABLE OPTIONS: [default, preset-1, preset-2, preset-3, preset-4, preset-5]
				//            layout: 'preset-5' // AVAILABLE OPTIONS: [default, preset-1, preset-2, preset-3, preset-4, preset-5]
		});
		AvaCart.Widgets.customSortedCurrencies();
		AvaCart.Widgets.quantityField.initialize();
		AvaCart.Widgets.purchaseAsGift.initialize();
		AvaCart.Widgets.downloadInsuranceService.initialize({
			addLabel: __order_widgets.ADD_LABEL || 'Add to cart'
		});
		AvaCart.Widgets.customBackupCDDesign.initialize({
			addLabel: __order_widgets.ADD_LABEL || 'Add to cart'
		});
		AvaCart.Widgets.coupon.before = function () {
			$('#order__coupon__input').after('<div class="coupon-placeholder" />');
		};
		AvaCart.Widgets.coupon.initialize({
			placeholder: '.coupon-placeholder'
		});
		AvaCart.Widgets.removeProducts.initialize();
		AvaCart.Widgets.paymentOptions.before = function () {
			$('.order__checkout__payment__content').prepend('<tr clas="payment-options-container"><td class="payment-options-label">' + $('.payment__title').text() + ': </td><td class="payment-options-wrapper"></td></tr>');
			$('#tiCNumber').after('<div class="payment-options-card-icons" />');
		};
		AvaCart.Widgets.paymentOptions.paymentOptionChangeCallback = function () {
            $('.credit-card-container').hide();
            if (_t_settings && _t_settings.accepted_cards && _t_settings.accepted_cards.hasOwnProperty($('#payment').val())) {
                $('.credit-card-container').show();
                $('.cvv-image-helper').show();
                $('.amex-cvv-image-helper').hide();
                if ($('#payment').val() === '4') { // AMEX CARD
                    $('.cvv-image-helper').hide();
                    $('.amex-cvv-image-helper').show();
                }
            }
            if (avaPage.isExpressCheckout && $('#payment').val() === '8') {
                $('#order__checkout__billing__data').hide();
            } else {
                $('#order__checkout__billing__data').show();
            }
        };
		AvaCart.Widgets.paymentOptions.initialize({
			placeholder: '.payment-options-wrapper',
			autoDetectIconsPlaceholder: '.payment-options-card-icons',
            groupCards: false,
			optionsOrder: {
				remove: [9, 2],
                order: [8, 1, 15],
				countries: {
					222: {
						remove: [6, 4],
						order: [11, 15, 49]
					}
				}
			},
			// spriteSrc: 'https://secure.avangate.com/images/merchant/6531b32f8d02fece98ff36a64a7c8260/pay_options_new.css',
			// spriteClass: 'sprite-new sprite-32',

			spriteSrc: 'https://secure.avangate.com/images/merchant/6531b32f8d02fece98ff36a64a7c8260/avacart-payment-options-sprite-48.css',
			spriteClass: 'sprite-48',

			// spriteSrc: 'https://secure.avangate.com/images/merchant/6531b32f8d02fece98ff36a64a7c8260/sprite-64x44-new-style.css',
			// spriteClass: 'sprite-new sprite-64',
			cardIconsPlaceholder: '.payment-options-card-icons',
			cardIconsSpriteSrc: 'https://secure.avangate.com/images/merchant/6531b32f8d02fece98ff36a64a7c8260/avacart-payment-options-sprite-48.css',
			cardIconsSpriteClass: 'sprite-48',
			//            radioOptions: 2,
			//            showIcons: true,

			// === CUSTOM LAYOUTS ===
			/* -------- Settings for 'layout-1': -------- */
			radioOptions: 0,
			showRadioButtons: false,
			showIcons: true,
			/* -------- Settings for 'layout-2': -------- */
			//            radioOptions: 2,
			//            showRadioButtons: false,
			//            showIcons: true,
			/* -------- Settings for 'layout-3': -------- */
			//            radioOptions: 3,
			//            showRadioButtons: true,
			//            showIcons: false,
			/* -------- Settings for 'layout-4': -------- */
			//            showRadioButtons: true,
			//            showLabels: false,
			//            showIcons: true,
			//            spriteSrc: 'https://secure.avangate.com/images/merchant/6531b32f8d02fece98ff36a64a7c8260/avacart-payment-options-sprite-32.css',
			//            spriteClass: 'sprite-32',
			/* -------- Settings for 'layout-5': -------- */
			//            radioOptions: 3,
			//            showIconsAfterText: true,
			//            showRadioButtons: false,
			//            cardIconsSpriteSrc: 'https://secure.avangate.com/images/merchant/6531b32f8d02fece98ff36a64a7c8260/avacart-payment-options-sprite-32.css',
			//            cardIconsSpriteClass: 'sprite-32',

			layout: 'layout-1'
		});
		custom_person_company({
			design: 'radio'
		});
        
		AvaCart.Widgets.shortCurrencySelector.initialize({
            placeholder: '.products-header-currency .select-box'
        });
	}
};

/**
 * Finish Page
 */
AvaCart.Callbacks.finishPage = {
	condition: (avaPage.isFinishPage),
	callback: function () {
        AvaCart.Widgets.fixRetryButton.initialize({
            htmlURL: $('#frmFinish').attr('action')
        });
	}
};

/**
 * Finish Page
 */
AvaCart.Callbacks.finishPage = {
	condition: (avaPage.isFinishPage && $('body').hasClass('page-order-complete')),
	callback: function () {
        AvaCart.Widgets.crossSelling.before = function () {
            $('#order__finish__finish__order').after('<div id="order__cross__sell__contents" class="cross__sell__products__listing products col-md-12"></div>');
        };
        AvaCart.Widgets.crossSelling.initialize({
            //            showProductDescription: true,
            crossSellProducts: cross_sell_products,
            displayCurrency: omniture_vars.DISPLAY_CURRENCY,
            billingCurrency: omniture_vars.BILLING_CURRENCY,
            placeholder: '#order__cross__sell__contents',
            maxProductsPerRow: 4,

            headerLabel: $('.order__crossselling_title').text() || 'We also recommend',
            addToCartLabel: $('.cross__sell__delimiter input.submit-button').val() || (typeof __order_widgets !== 'undefined' ? __order_widgets.ADD_LABEL : 'Add to cart') || 'Add to cart',

            // widgetClass: 'col-md-3',
            layout: 'display-horizontal' // AVAILABLE OPTIONS: [display-horizontal, display-vertical]
                //			layout: 'display-horizontal' // AVAILABLE OPTIONS: [display-horizontal, display-vertical]
        });
    }
};

jQuery(document).ready(function ($) {
	AvaCart.initialize(['global', 'checkoutPage', 'checkoutGracefulDegradation', 'finishPage', 'lastCallback']);
    
    if (navigator.userAgent.indexOf('Safari') > -1) {
        AvaCart.Widgets.pagePreloader.initialize({
            placeholder: '.page-preloader'
        });
    }
});

jQuery(window).load(function ($) {
	AvaCart.Widgets.pagePreloader.initialize({
        placeholder: '.page-preloader'
    });
});