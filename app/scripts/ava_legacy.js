/**
 * Log
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @version 1.0.0
 * @since 1.0.0
 * @param {string} msg
 * @param {string} method
 *
 */
function avaLog() {
    'use strict';
    try {
        var args = [].slice.call(arguments, 0);
        return (typeof avaDebug != 'undefined' && avaDebug || ($ && $.cookie ? $.cookie('avaDebug') : false) && arguments.length ? (typeof console != 'undefined' ? console.log.apply(console, args) : alert(args.toSource())) : false);
    } catch (error) {
        try {
            console.log(error);
        } catch (error) {}
    }
}
/**
 * Append Image
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @since 1.0.0
 *
 * @param {string} src    Source of the image.
 * @param {object} targetTag Where to append the image.
 */
function avaAppendImg(src, targetTag) {
    try {
        var targetTag = targetTag || 'body';
        target = document.getElementsByTagName(targetTag)[0],
        img = document.createElement('img'),
        img.setAttribute('height', 1);
        img.setAttribute('width', 1);
        img.setAttribute('src', src);
        target.appendChild(img);
    } catch (error) {
        avaLog(error);
    }
}

/**
 * Append Script
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @since 1.0.0
 *
 * @param {string} src    Source of the script.
 * @param {string} targetTag Where to append the script.
 */
function avaAppendScript(src, targetTag) {
    try {
        var targetTag = targetTag || 'head';
        target = document.getElementsByTagName(targetTag)[0],
        script = document.createElement('script'),
        script.setAttribute('type', 'text/javascript');
        script.setAttribute('src', src);
        target.appendChild(script);
        return true;
    } catch (error) {
        avaLog(error);
        return false;
    }
}

/**
 * Append Style
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @since 1.0.0
 *
 * @param {string} src    Source of the style.
 * @param {string} targetTag Where to append the style.
 */
function avaAppendStyle(src, targetTag) {
    try {
        var targetTag = targetTag || 'head';
        target = document.getElementsByTagName(targetTag)[0],
        link = document.createElement('link'),
        link.setAttribute('type', 'text/css');
        link.setAttribute('rel', 'stylesheet');
        link.setAttribute('href', src);
        target.appendChild(link);
        return true;
    } catch (error) {
        avaLog(error);
        return false;
    }
}

/**
 * Object Size
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 * @param {object}
 *
 * @return {number} returns the number object properties;
 */
function avaObjSize(obj) {
    try {
        var count = 0;
        for (i in obj) {
            if (obj.hasOwnProperty(i)) {
                count++;
            }
        }
        return count;
    } catch (error) {
        avaLog(error);
    }
}

/**
 * Get Url Parameter
 *
 * @version 1.0.0
 * @since 1.0.0
 *
 * @param {string} name
 *
 * @return {string}
 */
function avaGUP(query) {
    try {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [undefined, ""])[1].replace(/\+/g, '%20')) || '';
    } catch (error) {
        avaLog(error);
        return '';
    }
}

/**
 * Input Change Type Attribute
 *
 * @version 1.0.0
 *
 * @since   1.0.0
 *
 * @param   {string} x  is the <input/> element
 * @param   {string} type is the type you want to change it to.
 */
function avaInputChangeType(element, type) {
    try {
        element = $(element);
        if (typeof element.prop != 'undefined' && element.prop('type') == type) return element; //That was easy.
        try {
            return element.prop('type', type); //Stupid IE security will not allow this
        } catch (e) {
            //Try re-creating the element (yep... this sucks)
            //jQuery has no html() method for the element, so we have to put into a div first
            var html = $("<div>").append(element.clone()).html();
            var regex = /type=(\")?([^\"\s]+)(\")?/; //matches type=teelementt or type="teelementt"
            //If no match, we add the type attribute to the end; otherwise, we replace
            var tmp = $(html.match(regex) == null ? html.replace(">", ' type="' + type + '">') : html.replace(regex, 'type="' + type + '"'));
            //Copy data from old element
            tmp.data('type', element.data('type'));
            var events = element.data('events');
            var cb = function(events) {
                return function() {
                    //Bind all prior events
                    for (i in events) {
                        var y = events[i];
                        for (j in y) tmp.bind(i, y[j].handler);
                    }
                }
            }(events);
            element.replaceWith(tmp);
            setTimeout(cb, 10); //Wait a bit to call function
            return tmp;
        }
    } catch (error) {
        avaLog(error);
    }
}

/**
 * Load Scripts
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @version 1.0.0
 * @since   1.0.0
 *
 * @param   {object} scriptsToLoad
 * @return  {bool} True or Flase if the scripts are written in the DOM.
 */
function avaLoadScripts(scriptsToLoad) {
    try {
        for (i in scriptsToLoad) {
            if (typeof scriptsToLoad[i].condition != 'undefined') {
                if (scriptsToLoad[i].condition()) {
                    avaAppendScript(scriptsToLoad[i].src, scriptsToLoad[i].targetTag);
                }
            } else {
                avaAppendScript(scriptsToLoad[i].src, scriptsToLoad[i].targetTag);
            }
        }
        return true;
    } catch (error) {
        avaLog(error);
        return false;
    }
}

/**
 * Add Body Class
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 * @param {object} avaPage global or new empty object;
 * @param {object} $         jQuery;
 * @param {object} userAgent   user agent;
 */
(function(avaPage, $, userAgent) {
    try {
        if (typeof avaPageInit != 'undefined' && avaPageInit === false) {
            return false;
        }
        avaPage.path = window.location.pathname;
        avaPage.current = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1).replace('.php', '')
        avaPage.bodyClass = [avaPage.current];
        avaPage.isCart = window.location.pathname.search('cart.php') > -1;
        avaPage.isCheckoutPage = window.location.pathname.search('checkout.php') > -1;
        avaPage.isVerifyPage = window.location.pathname.search('verify.php') > -1;
        avaPage.isFinishPage = window.location.pathname.search('finish.php') > -1;
        avaPage.isAffiliatesPage = window.location.pathname.search('affiliates') > -1;
        avaPage.isPartnersPage = window.location.pathname.search('partners') > -1;
        avaPage.isSupportPage = window.location.pathname.search('support') > -1;
        avaPage.isDeliveryPage = window.location.pathname.search('delivery.php') > -1;
        avaPage.isUpgradePage = window.location.pathname.search('upgrade.php') > -1;
        avaPage.isSSO = (typeof _t_settings != 'undefined') ? ($('#payment_radio_NEW').length && _t_settings.hasOwnProperty('sso_cards') ? true : false) : false;
        avaPage.isTrial = (typeof omniture_vars != 'undefined') ? (omniture_vars.PURCHASE_TYPE == 'TRIAL') : false;
        avaPage.isTrialRegular = (typeof omniture_vars != 'undefined') ? (omniture_vars.PURCHASE_TYPE == 'MIXED' || omniture_vars.PURCHASE_TYPE == 'TRIAL') : false;
        avaPage.isComplete = (typeof omniture_vars != 'undefined') ? (omniture_vars.PURCHASE_COMPLETE == true) : false;
        avaPage.isFailed = (typeof omniture_vars != 'undefined') ? (omniture_vars.PURCHASE_COMPLETE == false) : false;
        avaPage.withReview = (typeof omniture_vars != 'undefined') ? (omniture_vars.CART_TYPE == 1) : false;
        avaPage.withoutReview = (typeof omniture_vars != 'undefined') ? (omniture_vars.CART_TYPE == 2) : false;
        avaPage.isExpressCheckout = (typeof _t_settings != 'undefined') ? (typeof _t_settings.cart_options.DESIGN_TYPE != 'undefined' && _t_settings.cart_options.DESIGN_TYPE == 1) : false;
        avaPage.isRtl = ($('html').attr('dir') == 'rtl');
        avaPage.browser = '';
        if (avaPage.path.indexOf('affiliates') > 0) {
            avaPage.bodyClass.push('page-affiliates');
        } else if (avaPage.path.indexOf('support') > 0) {
            avaPage.bodyClass.push('page-support');
        } else if (avaPage.path.indexOf('partners') > 0) {
            avaPage.bodyClass.push('page-partners');
        } else if (typeof _t_settings != 'undefined' && _t_settings.is_trial_order) {
            avaPage.bodyClass.push('page-trial');
        }
        if (avaPage.withReview) {
            avaPage.bodyClass.push('page-with-review');
        } else if (avaPage.withoutReview) {
            avaPage.bodyClass.push('page-without-review');
        }
        if (avaPage.isExpressCheckout) {
            avaPage.bodyClass.push('page-express-payments-checkout');
        }
        if (avaPage.isComplete && avaPage.isFinishPage) {
            avaPage.bodyClass.push('page-order-complete');
        } else if (avaPage.isFailed == true && avaPage.isFinishPage) {
            avaPage.bodyClass.push('page-order-failed');
        }
        if (userAgent.indexOf('Firefox') > -1) {
            avaPage.bodyClass.push('firefox');
            avaPage.browser = 'firefox';
        } else if (userAgent.indexOf('Chrome') == -1 && userAgent.indexOf('Safari') > -1) {
            avaPage.bodyClass.push('safari');
            avaPage.browser = 'safari';
        } else if (userAgent.indexOf('OPR') > -1) {
            avaPage.bodyClass.push('opera');
            avaPage.browser = 'opera';
        } else if (userAgent.indexOf('Chrome') > -1) {
            avaPage.bodyClass.push('chrome');
            avaPage.browser = 'chrome';
        } else if (userAgent.indexOf('MSIE') > -1) {
            avaPage.bodyClass.push('ie');
            if (userAgent.indexOf('MSIE 10') > -1) {
                avaPage.bodyClass.push('ie-10');
                avaPage.browser = 'ie-10';
            } else if (userAgent.indexOf('MSIE 9') > -1) {
                avaPage.bodyClass.push('ie-9');
                avaPage.browser = 'ie-9';
            } else if (userAgent.indexOf('MSIE 8') > -1) {
                avaPage.bodyClass.push('ie-8');
                avaPage.browser = 'ie-8';
            } else if (userAgent.indexOf('MSIE 7') > -1) {
                avaPage.bodyClass.push('ie-7');
                avaPage.browser = 'ie-7';
            }
        } else if (userAgent.indexOf('Trident') != -1 && userAgent.indexOf('rv:11') != -1) {
            avaPage.bodyClass.push('ie-11');
            avaPage.browser = 'ie-11';
        }
        if ($('#order__empty').length) {
            avaPage.bodyClass.push('empty-cart');
        }
        if (avaPage.isRtl) {
            avaPage.bodyClass.push('rtl');
        }
        if (avaPage.isSSO) {
            avaPage.bodyClass.push('sso');
        }
        $('body').addClass(avaPage.bodyClass.join(' '));
    } catch (error) {
        avaLog(error);
    }
})(window.avaPage = window.avaPage || {}, jQuery, window.navigator.userAgent);

/**
 * Parse Support Phones
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com) & Cezar Budulacu (cezar.budulacu@avangate.com)
 * @version 1.0.1
 *
 * @since   1.0.1
 *
 * @return  {string}
 */
function avaParseHotline() {
    try {
        window.noFooter = ($('.order__statement__value').length === 0);
        var pattLabel = /\(\D+\)/;
        var pattSupportLabel = /^[.^\d]*\(.+\)/;
        var hotline = {};
        hotline.HOTLINE = $.trim($('#order__statement__hotline__information .order__statement__label').html());
        hotline.phones = [];
        var hotlineText = $.trim($('.order__statement__value').html()).split(/<br\s{0,1}[\/]{0,1}>/i);
        for (var i = 0; i < hotlineText.length; i++) {
            var text = $.trim(hotlineText[i].replace(/(\r\n|\n|\r|\t)/gm, '').replace(/\u00A0/gm, ''));
            if (pattSupportLabel.test(text)) {
                hotline.SUPPORT = $.trim(text.replace(/[\(|\)]/g, ''));
            } else {
                var phoneNr = $.trim(text.replace(pattLabel, ''));
                var phoneLabel = $.trim(text.replace(phoneNr, '').replace(/[\(|\)]/g, ''));
                var phone = {};
                phone.country = phoneLabel;
                phone.number = phoneNr;
                hotline.phones.push(phone);
            }
        }
        window.avaHotline = hotline;
    } catch (error) {
        avaLog(error);
    }
}
jQuery(avaParseHotline());
/**
 * Normalize Cart
 *
 * @version 1.0.0
 * @since 1.0.0
 *
 * @param {object} $         jQuery;
 */
jQuery(function avaNormalizeCart($) {
    try {
        if (typeof avaNormalizeCartInit != 'undefined' && avaNormalizeCartInit === true) {
            return false;
        }
        try {
            // Remove or hide elements that are not relative or extra.
            $("[id='order__cart__contents']:eq(1)").removeAttr("id");
            $('.order__empty__row, .order__separator__row, .order__separator').remove();
            $('#order__payment__methods__details__link').hide();
            $('#order__finish__finish__order .order__finish__message__title, #order__finish__finish__order #order__statement__support,#order__finish__finish__order #order__statement__hotline__information').remove();
            $('#order__finish__footer div:first, #order__checkout__footer__data div:first, #order__verify__footer div:first').addClass('assistance');
            $('#order__page__payment #order__statement__support').parents('.order__box:first').addClass('assistance');
            $('.order__homepage__link').parents('table:first').addClass('back-to-shopping-wrapper');
        } catch (error) {
            avaLog(error);
        }
        try {
            for (i in omniture_vars.CART_PRODUCTS) {
                $('#order__products__' + omniture_vars.CART_PRODUCTS[i].ProductID).find('.order__box__title .order__box__aux2').html(omniture_vars.CART_PRODUCTS[i].ProductName);
            }
        } catch (error) {
            avaLog(error);
        }
        try {
            avaInputChangeType('#state2', 'text');
        } catch (error) {
            avaLog(error);
        }
        try {
            // Format Totals.
            $(".products-totals .products-total-right .products-total-row").each(function() {
                if ($(this).text() != '') {
                    var text_price, text_price_value, new_text_price;
                    text_price = $(this).text().split(':');
                    text_price_value = text_price[1].split('/');
                    if (text_price_value.length == 2) {
                        new_text_price = '<span class="prod-total-text">' + text_price[0] + ': </span><span class="order__display__total">' + text_price_value[0] + '/</span> <span class="order__billing__total">' + text_price_value[1] + '</span>';
                    } else {
                        new_text_price = '<span class="prod-total-text">' + text_price[0] + ': </span> <span class="order__billing__total">' + text_price_value[0] + '</span>';
                    }
                    $(this).html(new_text_price);
                }
            });
        } catch (error) {
            avaLog(error);
        }
        var bemClasses = [
            // Checkout Elements
            {
                current: '#order__header',
                newClass: 'container'
            }, {
                current: '#order__header__languages',
                newClass: 'language col-md-6 text-left'
            }, {
                current: '#order__header__languages__text',
                newClass: 'language__label'
            }, {
                current: '#order__header__languages .select',
                newClass: 'language__select'
            }, {
                current: '#order__header__currencies',
                newClass: 'currency col-md-6 text-right'
            }, {
                current: '#order__header__currencies__text',
                newClass: 'currency__label'
            }, {
                current: '#order__header__currencies .select',
                newClass: 'currency__select'
            }, {
                current: '#order__cart__contents',
                newClass: 'products'
            }, {
                current: '#order__cart__contents .order__box__title .order__box__aux2',
                newClass: 'products__title'
            }, {
                current: '#order__cart__contents .order__box__content .order__box__aux2',
                newClass: 'products__content'
            }, {
                current: '#order__cart__contents .order__listing__header td',
                newClass: 'products-content__title'
            }, {
                current: '#order__cart__contents .order__listing__row td',
                newClass: 'products-content__info'
            }, {
                current: '.order__listing__item__qty input, .order__listing__item__remove input,.order__text__field__coupon, #Update',
                newClass: 'products-content__input'
            }, {
                current: '.order__listing__item__qty input,.order__text__field__coupon',
                newClass: 'products-content__input--text'
            }, {
                current: '.order__listing__item__remove input',
                newClass: 'products-content__input--checkbox'
            }, {
                current: '#Update',
                newClass: 'products-content__input--button'
            }, {
                current: '.products-billing-txt',
                newClass: 'products-content__billing'
            }, {
                current: '.products-total-right',
                newClass: 'products-content__totals'
            }, {
                current: '.products-subtotal',
                newClass: 'products-content__subtotal'
            }, {
                current: '.products-totalvat',
                newClass: 'products-content__totalvat'
            }, {
                current: '.products-total',
                newClass: 'products-content__total'
            }, {
                current: '.order__vat__note',
                newClass: 'products-content__vat'
            }, {
                current: '.order__homepage__link',
                newClass: 'back-to-shopping-link'
            }, {
                current: '#order__checkout__billing__data',
                newClass: 'billing'
            }, {
                current: '#order__checkout__billing__data .order__box__title .order__box__aux2',
                newClass: 'billing__title'
            }, {
                current: '#order__checkout__billing__data .order__box__content .order__box__aux2',
                newClass: 'billing__content'
            }, {
                current: '#order__checkout__billing__data .order__help_billing',
                newClass: 'billing-content__reqtext'
            }, {
                current: '#order__checkout__billing__data .order__text__label',
                newClass: 'billing-content__label'
            }, {
                current: '#order__checkout__billing__data .order__text__label__mandatory',
                newClass: 'billing-content__label--mand'
            }, {
                current: '#order__checkout__billing__data input',
                newClass: 'billing-content__input'
            }, {
                current: '#order__checkout__billing__data select',
                newClass: 'billing-content__select'
            }, {
                current: '#order__checkout__payoptions__data',
                newClass: 'payment'
            }, {
                current: '#order__checkout__payoptions__data .order__box__title .order__box__aux2',
                newClass: 'payment__title'
            }, {
                current: '#order__checkout__payoptions__data .order__box__content .order__box__aux2',
                newClass: 'payment__content'
            }, {
                current: '#order__checkout__payoptions__data .order__help_billing',
                newClass: 'payment-content__reqtext'
            }, {
                current: '#order__checkout__payoptions__data .order__text__label',
                newClass: 'payment-content__label'
            }, {
                current: '#order__checkout__payoptions__data .order__text__label__mandatory',
                newClass: 'payment-content__label--mand'
            }, {
                current: '#order__checkout__payoptions__data input',
                newClass: 'payment-content__input'
            }, {
                current: '#order__checkout__payoptions__data select',
                newClass: 'payment-content__select'
            }, {
                current: '.submit input',
                newClass: 'submit__input'
            },
            // Footer Elements
            {
                current: '.assistance .order__box__content .order__box__aux2 ',
                newClass: 'assistance__content'
            }, {
                current: '.order__finish__message__title',
                newClass: 'assistance__title'
            },
            // Veify Page Elements
            {
                current: '#order__delivery',
                newClass: 'delivery'
            }, {
                current: '#order__delivery .order__box__title .order__box__aux2',
                newClass: 'delivery__title'
            }, {
                current: '#order__delivery .order__box__content .order__box__aux2',
                newClass: 'delivery__content'
            }, {
                current: '#order__delivery b,',
                newClass: 'delivery-content__title'
            }, {
                current: '#order__ccprocess__form',
                newClass: 'review'
            }, {
                current: '#order__ccprocess__form .order__box__title .order__box__aux2',
                newClass: 'review__title'
            }, {
                current: '#order__ccprocess__form .order__box__content .order__box__aux2',
                newClass: 'review__content'
            }, {
                current: '#order__ccprocess__form  .order__text__label',
                newClass: 'review-content__label'
            }, {
                current: '#order__ccprocess__form input',
                newClass: 'review-content__input'
            }, {
                current: '#order__ccprocess__form input',
                newClass: 'review-content__input--submit'
            }, {
                current: '#order__ccprocess__form select',
                newClass: 'review-content__select'
            }
        ];
        for (i in bemClasses) {
            try {
                $(bemClasses[i].current).addClass(bemClasses[i].newClass);
            } catch (error) {
                avaLog(error);
            }
        }
        try {
            // Footer
            $('#order__statement__support').after($('#order__statement'));
            $('.assistance .order__box__content .order__box__aux2').before($('.order__finish__message__title'));
            $('#order__statement__hotline__information').wrap('<div class="assistance-content__right col-md-4"></div>');
            $('.assistance__content').children().wrapAll('<div class="assistance-content__left col-md-8"></div>');
            $('.assistance-content__left').after($('.assistance-content__right'));
            $('.assistance-content__left').prepend($('.assistance__title'));
            $('.assistance__content').append('<div class="assistance-content__bottom col-md-12"></div>');
            $('.assistance-content__bottom').append($('#order__processedby, #order__privacy'));
            $('#footer .container .row').append($('.assistance'));
            // // Submit Button
            // $('.order__finish__button').parents('.tmp-li-fix:first').addClass('submit__container');
        } catch (error) {
            avaLog(error);
        }
        try {
            if (typeof avaHotline != 'undefined') {
                $('#order__statement__hotline__information').html('<h3 class=hotline__title>' + avaHotline.HOTLINE + '</h3><div class="hotline__content"></div>');
                $.each(avaHotline.phones, function(i, phone) {
                    $('.hotline__content').append('<div class="hotline-content"><span class="hotline-content__nr">' + (phone.number) + ' </span><span class="hotline-content__label">(' + phone.country + ')</span></div>');
                });
                if (avaHotline.SUPPORT) {
                    $('.hotline__content').append('<div class="hotline-support">' + avaHotline.SUPPORT + '</div>');
                }
            }
        } catch (error) {
            avaLog(error);
        }
    } catch (error) {
        avaLog(error);
    }
});
jQuery(function avaIconsFallbackIE7($) {
    try {
        if (typeof avaIconsFallbackIE7Init != 'undefined' && avaIconsFallbackIE7Init === false) {
            return false;
        }
        if ($.browser.msie && $.browser.version == '7.0') {
            var fontMapping = {
                'coupon': 'a',
                'phone': 'b',
                'placeorder': 'c',
                'refresh': 'd',
                'lifebuoy': 'e',
                'paypal': 'f',
                'search-find': 'g',
                'align-justify': 'h',
                'mail': 'i',
                'check': 'j',
                'close': 'k',
                'trash-bin': 'l',
                'home': 'm',
                'download': 'n',
                'cloud-download': 'o',
                'cd': 'p',
                'gift': 'q',
                'lock': 'r',
                'credit-card': 's',
                'left-open': 't',
                'down-open': 'u',
                'up-open': 'v',
                'right-open': 'w',
                'exclamation-circle': 'x',
                'info-circled': 'y'
            };
            $('[class^="icon-"], [class*=" icon-"]').each(function(i) {
                var $this = $(this);
                $this.prepend('<i id="pseudo-' + i + '" class="ie-before">' + fontMapping[$this.attr('class').match(/icon-\w+/g)[0].replace('icon-', '')] + '</i>');
            });
        }
    } catch (error) {
        avaLog(error);
    }
});
/**
 * Transform buttons elements into a tag.
 *
 * @version 1.0.0
 * @since 1.0.0
 *
 * @param {object} $ jQuery
 */
function avaOptimizeButtons() {
    try {
        if (typeof avaOptimizeButtonsInit != 'undefined' && avaOptimizeButtonsInit === true) {
            return false;
        }
        $('#Update').hide();
        $('input[type="button"],input[type="submit"],button').each(function(i) {
            var $this = $(this);
            var buttonId = 'btn' + i + '--btn'; // for inputs without an ID
            if ($this.attr('id')) { // for inputs that have an ID attribute
                if ($('[id=' + $this.attr('id') + ']').length > 1) { // multiple elements with the same ID
                    buttonId = $this.attr('id') + i + '--btn';
                } else {
                    buttonId = $this.attr('id') + '--btn';
                }
            }
            $this.hide();
            var buttonClasses = [];
            if ($this.hasClass('order__finish__button') || $this.hasClass('submit-super') || $this.hasClass('submit-button')) {
                buttonClasses.push('btn-cta btn-success btn-submit');
            }
            if ($this.hasClass('submit-large')) {
                buttonClasses.push('btn-lg');
            }

            // Create new a tag placeholder.
            $this.after('<button type="button" data-callback="" data-before="" data-trigger-click="true" class="btn btn-default ' + buttonClasses.join(' ') + '" id="' + buttonId + '">' + $this.val() + '</button>');

            // Bind the elements together.
            $('#' + buttonId).click(function () {
                try {
                    if (typeof $(this).data('before') === 'function') {
                        $(this).data('before')();
                    }
                } catch(error) {
                    avaLog(error);
                }
                try {
                    if ($(this).data('trigger-click') == true) {
                        $this.trigger('click');
                    }
                } catch(error) {
                    avaLog(error);
                }
                
                try {
                    if (typeof $(this).data('callback') === 'function') {
                        $(this).data('callback')();
                    }
                } catch(error) {
                    avaLog(error);
                }
            });
        });
    } catch (error) {
        avaLog(error);
    }
}
jQuery(avaOptimizeButtons);
/**
 * Custom Person Company
 *
 * @version 1.1.9
 *
 * @since 1.0.0
 *
 * @param {object} args { design: 'radio' }
 */
function custom_person_company(args) {
    //execute only when billing form is present.
    if ($.inArray(omniture_vars.PAGE_NAME, ['checkout', 'checkcart']) == -1) {
        return false;
    }
    //function
    var functions = {
        show_vatid: function(country_id) {
            var show_vatid = false;
            for (i = 0; i < _t_settings.eu_countries.length; i++) {
                if (country_id == _t_settings.eu_countries[i]) {
                    show_vatid = true;
                    break;
                }
            }
            return show_vatid;
        },
        hasBackupCd: function() {
            for (ii = 0; ii < omniture_vars.CART_PRODUCTS.length; ii++) {
                if (omniture_vars.CART_PRODUCTS[ii].ProductType == 'MEDIA') {
                    return true;
                }
            }
            return false;
        },
        isKonbini: function() {
            return $('#payment option:selected').val() == 42; //42
        },
        isPagBrasil: function() {
            var payOptId = $('#payment option:selected').val();
            var country = $('#billingcountry option:selected').attr('value')
            //return _t_settings.payoptions_terminals && _t_settings.payoptions_terminals[payOptId] === 'PAGBRASIL';
            return getTerminalType(payOptId, country) === 'PAGBRASIL';
        },
        isPurchaseOrder: function() {
            return $('#payment').val() == 11;
        },
        isShortForm: function() {
            return omniture_vars.CART_OPTIONS && omniture_vars.CART_OPTIONS.SHORT_FORM;
        }
    }
    // short form: dashes fix
    if (functions.isShortForm()) {
        if ($('#city').val() == '-') {
            $('#city').attr('value', '');
        }
        if ($('#address').val() == '-') {
            $('#address').attr('value', '');
        }
        if ($('#zipcode').val() == '-') {
            $('#zipcode').attr('value', '');
        }
    }
    //general
    var args = (args == undefined ? new Object : args);
    var tmp = '';
    var html_row = '';
    var tmp_val = '';
    //NOTE: don't change the order of the elements;
    var tr_rows = {
        company_check: ['#bill_company', '#fcode'],
        company_hide: ['#bill_company', '#fcode', '#fcode_text', '#bill_phone', '#bill_phone_info', '#bill_fax', 'tr.order__separator__row']
    };
    var country_id = $('#billingcountry option:selected').attr('value');
    var show_vatid = functions.show_vatid(country_id);
    var checkout_type = 'person';
    if (!args.design) {
        return false;
    }
    //by default hide company rows if all their <input> fields are empty
    for (ii = 0; ii < tr_rows.company_check.length; ii++) {
        tmp = $(tr_rows.company_check[ii]).find('input[type="text"]');
        if (tmp && $(tmp).val() && $(tmp).val() != '') {
            checkout_type = 'company';
            break;
        } else {
            checkout_type = 'person';
        }
    }
    //hide if company
    if (checkout_type == 'person') {
        for (ii = 0; ii < tr_rows.company_hide.length; ii++) {
            $(tr_rows.company_hide[ii]).css('display', 'none');
        }
        if (functions.isPurchaseOrder()) {
            $('#bill_company').show();
        }
    }
    //radio
    if (args.design == 'radio') {
        //html
        html_row = '<tr id="order_person_company">';
        html_row += '<td class="order__checkout__form__label"><span class="order__checkout__form__mandatory order__text__label order__text__label__mandatory">' + __order_person_company.RADIO_LABEL + ':</span></td>';
        html_row += '<td class="order__checkout__form__input">';
        html_row += '<input type="radio" id="person_radio" name="checkout_type" value="person" ' + (checkout_type == 'person' ? 'checked' : '') + ' /> ';
        html_row += '<label for="person_radio">' + __order_person_company.PERSON + '</label>';
        html_row += '<input type="radio" id="company_radio" name="checkout_type" value="company" ' + (checkout_type == 'company' ? 'checked' : '') + ' /> ';
        html_row += '<label for="company_radio">' + __order_person_company.COMPANY + '</label>';
        html_row += '</td>';
        html_row += '</tr>';
        //append html
        $('#order__checkout__billing__data .order__checkout__billing__content tr:first').before(html_row);
    }
    //checkbox
    if (args.design == 'checkbox') {
        //html
        html_row = '<tr id="order_person_company">';
        html_row += '<td class="order__checkout__form__label">&nbsp;</td>';
        html_row += '<td class="order__checkout__form__input"><input type="checkbox" value="company" name="checkout_type" id="checkout_type" ' + (checkout_type == 'company' ? 'checked' : '') + ' /> <label for="checkout_type" class="label_person_company"><span>' + __order_check_if_company + '</span></label></td>';
        html_row += '</tr>';
        //append html
        $("tr.order__separator__row:first").after(html_row);
    }
    //bind
    $("input[name='checkout_type']").live(($.browser.msie ? "click" : "change"), function() {
        country_id = $('#billingcountry option:selected').attr('value');
        show_vatid = functions.show_vatid(country_id);
        if (args.design == 'radio') {
            checkout_type = $(this).val();
        }
        if (args.design == 'checkbox') {
            if ($(this).attr('checked') == true) {
                checkout_type = 'company';
            } else {
                checkout_type = 'person';
            }
        }
        if (checkout_type == 'person') {
            //hide
            for (ii = 0; ii < tr_rows.company_hide.length; ii++) {
                $(tr_rows.company_hide[ii]).css('display', 'none');
            }
            if (functions.isKonbini() || functions.isPagBrasil() || functions.hasBackupCd()) {
                //show phone + phone info
                $("#bill_phone").show();
                $("#bill_phone_info").show();
            }
            if (functions.isShortForm()) {
                $('#bill_address1').addClass('SHORT_FORM').hide();
            }
            if (functions.isPurchaseOrder()) {
                $('#bill_company').show();
            }
        }
        if (checkout_type == 'company') {
            //show
            for (ii = 0; ii < tr_rows.company_hide.length; ii++) {
                $(tr_rows.company_hide[ii]).css('display', '');
            }
            //bind get country
            //if not Romania hide these fields. (same as in order.js - ShowExtraFields(country))
            if (parseInt(country_id) != 237) {
                $('#cbank').css('display', 'none');
                $('#cbankaccount').css('display', 'none');
                $('#rnumber').css('display', 'none');
            }
            //show VAT ID only for EU countries.
            if (show_vatid == false) {
                $('#fcode').hide();
                $('#fcode_text').hide();
            }
            if (functions.isShortForm()) {
                $('#bill_address1').removeClass('SHORT_FORM').show();
            }
        }
    });
    //fix: bind #billingcountry to prevent VAT Id from showing up if the customer is not a company
    tmp = $('#billingcountry');
    if (tmp.length > 0) {
        $(tmp).bind('change', function() {
            country_id = $('#billingcountry option:selected').attr('value');
            show_vatid = functions.show_vatid(country_id);
            if (checkout_type == 'person') {
                $('#fcode').hide();
                $('#fcode_text').hide();
            }
            if (checkout_type == 'company' && show_vatid == true) { // && _t_settings.trans.ord_interface == 'AVANGATE'
                $('#fcode').show();
                $('#fcode_text').show();
            }
        });
    }
    if (functions.hasBackupCd() || functions.isKonbini() || functions.isPagBrasil()) {
        //show phone + phone info
        $("#bill_phone").show();
        $("#bill_phone_info").show();
    } else {
        //hide sameaddr question
        //$("#sameaddr").hide(); //Show because of the Box feature
    }
}
/**
 * Auto Submit Disk Backup
 *
 * @version 1.0.0
 * @since   1.0.0
 */
function auto_submit_dis_bak() {
    $('.order__dis__option input:first, .order__backupcd__option input:first').live('click', function() {
        $('form#frmCheckout').submit();
    });
}
/**
 * Slugify
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @version 1.0.0
 * @since   1.0.0
 * @param   {string} text
 *
 * @return  {string}
 */
function avaSlugify(text) {
    return text.toString().toLowerCase().replace(/\s+/g, '-') // Replace spaces with -.
    .replace(/[^\w\-]+/g, '') // Remove all non-word chars.
    .replace(/\-\-+/g, '-') // Replace multiple - with single -
    .replace(/^-+/, '') // Trim - from start of text.
    .replace(/-+$/, ''); // Trim - from end of text.
}
/**
 * Remove Faulty Scripts
 *
 * @author Alexandru Salajan (alexandru.salajan@avangate.com)
 * @version 1.0.0
 * @since 1.0.0
 */
(function avaRemoveFaultyScripts($) {
    try {
        $('.helpClass script').remove();
    } catch (error) {
        avaLog(error);
    }
})(jQuery);
/**
 * Parse Order Privacy Links
 *
 * @author Alexandru Salajan (alexandru.salajan@avangate.com)
 * @version 1.0.0
 * @since 1.0.0
 */
(function avaParseOrderPrivacyLinks($) {
    try {
        var orderPrivacyLinks = $('#order__privacy a');
        $('#order__privacy').empty();
        orderPrivacyLinks.each(function(index, Elem) {
            $(this).addClass('order-privacy-link').appendTo($('#order__privacy'));
            if (index === (orderPrivacyLinks.length - 1)) return;
            $('#order__privacy').append($('<span class="order-privacy-link-separator">|</span>'));
        });
    } catch (error) {
        avaLog(error);
    }
})(jQuery);

/**
 * Adjust URL Paramenters
 *
 * @author Alexandru Salajan (alexandru.salajan@avangate.com)
 * @version 1.0.0
 * @since 1.0.0
 */
(function adjustURLParameters($) {
    try {
        if ( !(avaPage.current === 'checkout') ) return false;
        var currentURL = window.location.href;
        if ( (typeof omniture_vars != 'undefined') && (typeof omniture_vars.CART_OPTIONS != 'undefined') ) {
            if (typeof omniture_vars.CART_OPTIONS.CARD == 'undefined') {
                currentURL += '&CARD=1';
                if (typeof omniture_vars.CART_OPTIONS.CART == 'undefined') {
                    currentURL += '&CART=1';
                }
                window.location.replace(currentURL);
            }
        }
    } catch (error) {
        avaLog(error);
    }
})(jQuery);

/**
 * Adjust Copyright
 *
 * @author Alexandru Salajan (alexandru.salajan@avangate.com)
 * @version 1.0.0
 * @since 1.0.0
 */
(function adjustCopyright($) {
    try {
        if ($('.signup_form_footer').length === 0) return false;
        var currentYear = new Date().getFullYear();
        $('.signup_form_footer').html(function(i, html) {
            return html.replace('2011', currentYear);
        });
    } catch (error) {
        avaLog(error);
    }
})(jQuery);

/**
 * Reset "Full name" input value
 *
 * @author Alexandru Salajan (alexandru.salajan@avangate.com)
 * @version 1.0.0
 * @since 1.0.0
 */
jQuery(function resetFullNameInputValue($) {
    try {
        if ( !(avaPage.current === 'checkout') ) return false;
        if ( $('#fullname').val() === ' ' ) {
            $('#fullname').val('');
        }
    } catch (error) {
        avaLog(error);
    }
});

var upSell = {
    callbacks: {
        optimizeUpSellButtons: avaOptimizeButtons 
    },
    initialize: function() {
        try {
            for (i in this.callbacks) {
                if (typeof this.callbacks[i] === 'function') {
                    this.callbacks[i]();
                }
            }
        } catch (error) {
            avaLog(error);
        }
    }
}
var avng8_upsell_callbacks = {
    ajax_success: function (obj) {
        upSell.initialize();
    }
 }

var xSell = {
    callbacks: {
        optimizeXSellButtons: function () {
            $('#cross__sell__finish__confirm input[type="submit"]').removeClass('submit-button').addClass('btn btn-default btn-cta btn-success btn-submit btn-lg');
        }
    },
    initialize: function() {
        try {
            for (i in this.callbacks) {
                if (typeof this.callbacks[i] === 'function') {
                    this.callbacks[i]();
                }
            }
        } catch (error) {
            avaLog(error);
        }
    }
}
cross_sell_load_callback = {
    html_loaded: function (obj) {
        xSell.initialize();
    }
}