/**
 * Cart Steps
 *
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 */
AvaCart.Widgets.cartSteps = {
/* ==========================================================================
   SETTINGS
   ========================================================================== */
    settings: {
        placeholder     : null,

        steps           : null,
        checkedIconClass: 'icon-check',
		noOfSteps		: 4,
		startFromStep	: false,
		removeSteps		: false,
		showOnPages		: false,

		showLinks		: false,
        showStepBox     : false,
        showStepIcon    : true,
        
        colorCheckedSteps: false,
        colorCurrentStep : false,
        
		currentStep		: 1,
        
        showLabelsOnTop : true,
        barStyle        : 'solid',
        layout          : '',
        widgetClass     : null
    },
    template: null,

/* ==========================================================================
   FUNCTIONS
   ========================================================================== */
    functions: {
        initialSetup: function (settings) {
            // show on pages
            if(settings.showOnPages && $.inArray(avaPage.current, settings.showOnPages) == -1) {
                return false;
            }
            
            // start from step
            if(settings.startFromStep && settings.startFromStep <= settings.noOfSteps) {
                for(i in settings.steps) {
                    if(i < settings.startFromStep) {
                        delete settings.steps[i];
                        --settings.noOfSteps;
                    }
                }
            }

            // if without review -> remove step 3
            if(typeof omniture_vars !== undefined) {
                if(omniture_vars.CART_TYPE == 2) {
                    delete settings.steps[3];
                    --settings.noOfSteps;
                }
            }

            // remove steps
            if(settings.removeSteps) {
                for(i in settings.steps) {
                    if($.inArray(i,settings.removeSteps) > -1) {
                        delete settings.steps[i];
                        --settings.noOfSteps;
                    }
                }
            }

            // set current step
            if(typeof avaPage !== undefined) {
                settings.currentStep = 1;
                for(i in settings.steps) {
                    if(settings.steps[i].page == avaPage.current) {
                        break;
                    }
                    settings.currentStep++;
                }
            }

            return true;
        }
    },
/* ==========================================================================
   RENDER TEMPLATE
   ========================================================================== */
    render: function () {
    /* ===================
       Initial setup
       =================== */
        if (!this.functions.initialSetup(this.settings)) return false;
        
    /* ===================
       Template context
       =================== */
        var context = {
            settings: this.settings
        }
        
        var html = nunjucks.renderString(this.template, context);
        $(this.settings.placeholder).append(html);
    },

/* ==========================================================================
   EVENTS
   ========================================================================== */
    events: function () {},
/* ==========================================================================
   DESTROY
   ========================================================================== */
    destroy: function () {},
/* ==========================================================================
   BEFORE
   ========================================================================== */
    before: function () {},
/* ==========================================================================
   CALLBACK
   ========================================================================== */
    callback: function () {},

/* ==========================================================================
   INITIALIZATION
   ========================================================================== */
    initialize: function (options) {
        if (typeof __order_steps == 'undefined') return false;
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}