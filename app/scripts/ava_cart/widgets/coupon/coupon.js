/**
 * Coupon
 *
 * @author Adrian Staniloiu <adrian.staniloiu@avangate.com>
 * @type {Object}
 */
AvaCart.Widgets.coupon = {
    settings: {
        placeholder: null,
        checkbox: true,
        I_HAVE_DISCOUNT_COUPON: null,
        DISCOUNT_COUPON: null
    },
    template: null,
    render: function () {
      $(this.settings.placeholder).append(nunjucks.renderString(this.template, {
        I_HAVE_DISCOUNT_COUPON: this.settings.I_HAVE_DISCOUNT_COUPON || __order_widgets.I_HAVE_DISCOUNT_COUPON,
        DISCOUNT_COUPON: this.settings.DISCOUNT_COUPON || __order_widgets.DISCOUNT_COUPON,
        settings: this.settings
      }));
    },
    events: function () {
        var self = this;
        $('#order__coupon__input').hide();
        if (self.settings.checkbox) {
            $('.coupon__form').hide();
        }
        if ($('#order__coupon__input').find('.error').length) {
          $('.coupon__form').show().find('[name="coupon_fake"]').addClass('order__text__field__error');
          $('#coupon-checkbox').attr('checked', true);
        }
        $('[name="coupon_fake"]').bind('keyup blur', function() {
            $(this).removeClass('order__text__field__error');
            $('[name="coupon"]').val(this.value);
        });
        $('#coupon-checkbox').change(function () {
            $('.coupon__form').slideToggle();
        });
        $('#coupon-apply').click(function() {
            if (!$.trim($('[name="coupon_fake"]').val())) {
                $('[name="coupon_fake"]').addClass('order__text__field__error');
                return false;
            }
            $('#Update').trigger('click');
        });
    },
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            if (typeof omniture_vars.DISCOUNT_COUPON === 'undefined') return;
            $.extend(this.settings, options); // Extend default settings.
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
