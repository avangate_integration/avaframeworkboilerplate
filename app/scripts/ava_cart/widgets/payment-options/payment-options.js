/**
 * Payment Options
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 */

// TODO: Checkout with cart functionalities (grupCards false)
AvaCart.Widgets.paymentOptions = {
	settings: {
		placeholder: null,
		spriteClass: 'sprite',
		spriteSrc: null,
		groupCards: true,
		showGroupedCardsIcons: true, // Works only if "groupCards" and "showIcons" is "true".
		autogetCardTypes: true, // If "groupCards" is "true" "autogetCardTypes" will be considered "true".
		cardsLabel: 'Credit Cards',
		radioOptions: 3,
		showRadioButtons: true,
		showLabels: true,
		showIcons: true,
		showOptionTitle: true,
		selectOtherMethods: 'Select other methods',
		cardIconsPlaceholder: null,
		cardIconsSpriteSrc: null,
		cardIconsSpriteClass: null,
		layout: 'default', // AVAILABLE OPTIONS: ['default', 'layout-1', 'layout-2', 'layout-3', 'layout-4', 'layout-5']
		widgetClass: null,
		optionsOrder: {},
		countryHasChanged: false
	},
	template: null,
	functions: {
		/**
		 * Change Payment Options Order
		 *
		 * @version 1.0.0
		 *
		 * @since   1.0.0
		 */
		changePaymentOptionsOrder: function (settings) {
			if (typeof settings.optionsOrder !== 'undefined') {
				var billingcountry = $('#billingcountry').val();
				if (typeof settings.optionsOrder.order !== 'undefined' &&
					settings.optionsOrder.order instanceof Array &&
                    typeof settings.optionsOrder.countries !== 'undefined' &&
					(typeof settings.optionsOrder.countries[billingcountry] === 'undefined' ||
						typeof settings.optionsOrder.countries[billingcountry].order === 'undefined')) {
					var paymentOptionsOrder = settings.optionsOrder.order.slice().reverse();
					for (i in paymentOptionsOrder) {
						$('#payment').prepend($('#payment option[value="' + paymentOptionsOrder[i] + '"]'));
					}
					if (!settings.groupCards && ((omniture_vars.FIRST_CART_ACCESS == 1 && _t_settings.PAYID !== paymentOptionsOrder.slice(-1)) || settings.countryHasChanged)) {
						this.triggerPaymentChange(paymentOptionsOrder.slice(-1));
					} else {
						$('#payment').prepend($('#payment option:selected'));
					}
				}
				if (typeof settings.optionsOrder.countries !== 'undefined' &&
					settings.optionsOrder.countries.hasOwnProperty(billingcountry) &&
					typeof settings.optionsOrder.countries[billingcountry].order !== 'undefined' &&
					settings.optionsOrder.countries[billingcountry].order instanceof Array) {
					var countryPaymentOptionsOrder = settings.optionsOrder.countries[billingcountry].order.slice().reverse();
					for (i in countryPaymentOptionsOrder) {
						$('#payment').prepend($('#payment option[value="' + countryPaymentOptionsOrder[i] + '"]'));
					}
					if (!settings.groupCards && ((omniture_vars.FIRST_CART_ACCESS == 1 && _t_settings.PAYID !== countryPaymentOptionsOrder.slice(-1)) || settings.countryHasChanged)) {
						this.triggerPaymentChange(countryPaymentOptionsOrder.slice(-1));
					} else {
						$('#payment').prepend($('#payment option:selected'));
					}
				}
			}
		},
		/**
		 * Detect Cards
		 *
		 * @version 1.0.0
		 *
		 * @since   1.0.0
		 *
		 * @param   {string} input
		 *
		 * @return  {string} Card type if found in _t_settings.accepted_cards;
		 */
		getCardType: function (input) {
			var cardType;
			if (typeof input != 'string' || !input.length) {
				return false;
			}

			// VISA
			if (input.match(/^4[0-9]\d+$/)) {
				cardType = '1';
			}
			// Cart Bleue
			if (input.match(/^497[0-9]\d+$/)) {
				cardType = '17';
			}
			// MasterCard
			if (input.match(/^5[0-9]\d+$/)) {
				cardType = '1';
			}
			// Amex
			if (input.match(/^3[47]\d+$/)) {
				cardType = '4';
			}
			// Diners
			if (input.match(/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/)) {
				cardType = '5';
			}
			// Discover
			if (input.match(/^6(?:011)\d+$/)) {
				cardType = '14';
			} else if (input.match(/^6[0-9][0-9]\d+$/)) { // Visa
				cardType = '1';
			}
			// JCB
			if (input.match(/^(?:2131|1800|35\d{3})\d{11}$/)) {
				cardType = '6';
			}

			if (_t_settings.accepted_cards.hasOwnProperty(cardType)) {
				return cardType;
			} else {
				return false;
			}
		},
		/**
		 * Trigger "#payment" change for all flows.
		 *
		 * @version 1.0.0
		 *
		 * @since   1.0.0
		 *
		 * @param   {string} value
		 */
		triggerPaymentChange: function (value) {
			var value = (value === 'cc' && _t_settings && _t_settings.accepted_cards ? (function() { for (i in _t_settings.accepted_cards) { return i;  }})() : value);
			if (omniture_vars.DESIGN_TYPE == 1) {
				$('#payment_radio_' + value).trigger('click');
			} else {
				$('#payment').val(value).trigger('change');
			}
		},
		/**
		 * Create payment options array.
		 *
		 * @version 1.0.0
		 *
		 * @since   1.0.0
		 *
		 * @param   {bolean} groupCards
		 *
		 * @return  {array} Payment Options Array
		 */
		getPaymentOptionsArray: function (settings) {
			var paymentOptions = [],
				cardsOption = {
					id: 'cc',
					title: settings.cardsLabel,
					value: null,
					checked: false,
					cards: []
				};
			try {
				this.changePaymentOptionsOrder(settings);
			} catch (error) {
				avaLog(error)
			}
			$('#payment option').each(function (i, el) {
				var $el = $(el),
					billingcountry = $('#billingcountry').val();
				$el.text(function (i, text) {
					return text.replace('/Eurocard', '');
				});
				// Payment Options Order
				if (typeof settings.optionsOrder !== 'undefined') {
					if (typeof settings.optionsOrder.remove !== 'undefined' && $.inArray(parseInt(el.value), settings.optionsOrder.remove) > -1) {
						return;
					}
					if (typeof settings.optionsOrder.countries !== 'undefined' && settings.optionsOrder.countries.hasOwnProperty(billingcountry)) {
						if (typeof settings.optionsOrder.countries[billingcountry].remove !== 'undefined' && $.inArray(parseInt(el.value), settings.optionsOrder.countries[billingcountry].remove) > -1) {
							return;
						}
					}
				}
				//
				var option = {
					id: $el.val(),
					title: $el.text(),
					value: $el.val(),
					checked: $el.is(':checked')
				}
				if (settings.groupCards && _t_settings.accepted_cards.hasOwnProperty($el.val()) && $el.val() != '49') {
					if ($el.is(':selected')) {
						cardsOption.checked = $el.is(':selected');
						cardsOption.value = $el.val();
					}
					var card = {
						title: $el.text(),
						value: $el.val(),
						checked: $el.is(':selected')
					};
					cardsOption.cards.push(card);
				} else {
					paymentOptions.push(option);
				}
			});

			// Check if "cardsOption" has value, if not a set first value from cards.
			if (cardsOption.value == null && cardsOption.cards.length) {
				cardsOption.value = cardsOption.cards[0];
			}

			// If "groupCards" is true push and unshift paymentOptions.
			if (settings.groupCards) {
				paymentOptions.unshift(cardsOption);
			}

			// if (cardsOption.checked == false) {
			//     paymentOptions = (function(input) {
			//         var output = [],
			//             checkedOption = null;
			//         for (i in input) {
			//             if (input[i].checked) {
			//                 checkedOption = input[i];
			//             } else {
			//                 output.push(input[i]);
			//             }
			//         }
			//
			//         output.unshift(checkedOption);
			//
			//         return output;
			//     })(paymentOptions);
			// }

			return paymentOptions;
		},
		/**
		 * Get card icons.
		 */
		getCardIcons: function (cards, settings) {
			var output = [];
			for (i in cards) {
				output.push('<img class="payment-option-icon ' + (settings.cardIconsSpriteClass ? settings.cardIconsSpriteClass : settings.spriteClass) + ' pay_opt_' + cards[i].value + '" src="https://edge.avangate.net/images/spacer.gif" for="payment-option-radio-' + cards[i].value + '" title="' + cards[i].title + '">');
			}
			output.push('</div>');

			return output.join('');
		},
        /**
		 * Update Submit Button
		 *
		 * @version 1.0.0
		 *
		 * @since   1.0.0
		 */
		updateSubmitButton: function () {
            $('input.order__finish__button + button.btn-submit').text($('input.order__finish__button').val());
        }
	},
	render: function () {
		// Hide old select.
		$('#payment__methods').hide();

		var context = {
			settings: this.settings,
			paymentOptions: this.functions.getPaymentOptionsArray(this.settings)
		}
		// context.selectedOption = (function(paymentOptions){
		//     for (i in paymentOptions) {
		//         if (paymentOptions[i].checked == true) {
		//             return paymentOptions[i];
		//         }
		//     }
		// })(context.paymentOptions);
		var html = nunjucks.renderString(this.template, context);
		$(this.settings.placeholder).append(html);

		if (this.settings.spriteSrc) {
			avaAppendStyle(this.settings.spriteSrc);
		}
		if (this.settings.cardIconsPlaceholder && this.settings.groupCards) {
			if (this.settings.cardIconsSpriteSrc) {
				avaAppendStyle(this.settings.cardIconsSpriteSrc);
			}
			var cards = (function (input) {
				for (i in input) {
					if (input[i].id == 'cc') {
						return input[i].cards;
					}
				}
			})(this.functions.getPaymentOptionsArray(this.settings));

			$(this.settings.cardIconsPlaceholder).append(this.functions.getCardIcons(cards, this.settings));
		}
	},
	events: function () {
		var self = this;

		// Bind click event for select method.
		if ($('.payment-options-select').length) {
			$('#payment-options-select-selected-option').click(function (event) {
				stopEvent(event);
				$('.payment-options-select-list').slideToggle('fast', function () {
					$('#payment-options-select-selected-option').removeClass('focus');
					if ($(this).is(':visible')) {
						$('#payment-options-select-selected-option').addClass('focus');
					}
				});
			});
			$('html').click(function (event) {
				$('.payment-options-select-list').slideUp();
				$('#payment-options-select-selected-option').removeClass('focus');
			});
		}
		// Trigger payment options change callback.
		try {
            AvaCart.Widgets.selectBoxes.resetSelectBoxesSelectedValues();
            self.functions.updateSubmitButton();
			self.paymentOptionChangeCallback();
		} catch (error) {
			avaLog(error);
		}
		// Bind click event on payment option radio.
		$('.payment-option-radio').click(function (event) {
			event.stopPropagation();
			var $this = $(this);

			// Treat case for ".payment-options-select-list".
			if ($this.parents('.payment-options-select-list:first').length) {
				$('.payment-options-select-selected-option-value').html($this.parents('.payment-option:first').find('label').html());
				$('.payment-options-select-selected-option-value').parents('.payment-options-select:first').addClass('selected');
			} else {
				$('.payment-options-select-selected-option-value').html(self.settings.selectOtherMethods);
				$('.payment-options-select-selected-option-value').parents('.payment-options-select:first').removeClass('selected');
			}

			// Add "selected" class to ".payment-option".
			$('.payment-option').removeClass('selected')
			$this.parents('.payment-option:first').addClass('selected');

			// Trigger payment change.
			self.functions.triggerPaymentChange($this.data('payment-option-value'));

			// Trigger payment options change callback.
			try {
                AvaCart.Widgets.selectBoxes.resetSelectBoxesSelectedValues();
                self.functions.updateSubmitButton();
				self.paymentOptionChangeCallback();
			} catch (error) {
				avaLog(error);
			}
		});

		if ($('.payment-options-select-list > li.selected').length) {
			$('.payment-options-select-selected-option-value').html($('.payment-options-select-list > li.selected').find('label').html());
		}

		// SSO in cart case.
		if ($('#payment_radio_NEW').length && _t_settings.hasOwnProperty('sso_cards')) {
			if ($('#payment_radio_NEW').not(':checked')) {
				$(self.settings.placeholder).hide();
			}
			$('#existing_cards_radios_wrap .payment_radio_input').live('click', function (event) {
				if (event.target.id == 'payment_radio_NEW') {
					$(self.settings.placeholder).show();
				} else {
					$(self.settings.placeholder).hide();
				}
			});

		}

		// Bind auto detect cards.
		if (self.settings.groupCards && self.settings.autogetCardTypes) {
			$('#tiCNumber').addClass('card-number-input-autodetect ' + self.settings.cardIconsSpriteClass).bind('blur keyup', function () {
				if (self.functions.getCardType(this.value)) {
					if (self.settings.groupCards) {
						$('#payment-option-radio-cc').attr('checked', true);
						self.functions.triggerPaymentChange(self.functions.getCardType(this.value));
					} else {
						$('#payment-option-radio-' + self.functions.getCardType(this.value)).trigger('click');
					}
					$(self.settings.cardIconsPlaceholder).find('img').removeClass('active');
					$(self.settings.cardIconsPlaceholder).find('.pay_opt_' + self.functions.getCardType(this.value)).addClass('active');
				}
                
                // Trigger payment options change callback.
                try {
                    AvaCart.Widgets.selectBoxes.resetSelectBoxesSelectedValues();
                    self.functions.updateSubmitButton();
                    self.paymentOptionChangeCallback();
                } catch (error) {
                    avaLog(error);
                }
			});
		}

	},
	destroy: function () {
		$(this.settings.placeholder).empty();
		$(this.settings.cardIconsPlaceholder).empty();
	},
	before: function () {},
	callback: function () {},
	paymentOptionChangeCallback: function () {},
	initialize: function (options) {
		try {
			var self = this;
			$.extend(true, self.settings, options); // extend default settings
			self.before();
			self.render();
			self.events();
			self.callback();
			// Reinitialize payment options after "#billingcountry" change.
			$('#billingcountry').change(function () {
				self.settings.countryHasChanged = true;
				self.destroy();
				self.render();
				self.events();
			});
		} catch (error) {
			avaLog(error);
		}
	}
}
