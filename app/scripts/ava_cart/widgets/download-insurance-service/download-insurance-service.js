/**
 * Download Insurance Service
 *
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 */
AvaCart.Widgets.downloadInsuranceService = {
/* ==========================================================================
   SETTINGS
   ========================================================================== */
    settings: {
        placeholder : 'table.order__dis__option',
        
        showIcon    : true,
        btnIcon     : 'cloud-download',
        addLabel    : 'Add to cart',
        
        inputIDs    : []
    },
    template: null,

/* ==========================================================================
   FUNCTIONS
   ========================================================================== */
    functions: {
        initialSetup: function (settings) {
            var original_input, original_input_id;
            $(settings.placeholder).each( function (index, Element) {
                $(this).find('label').attr('for', '');
                
                original_input = $(this).find('td:first input');
                original_input_id = $(original_input).attr('id');
                settings.inputIDs.push(original_input_id);
                $(original_input).addClass('dis-original-input');
                
                // Remove old icon
                $(this).find('.order__dis__icon').parent('td').remove();
            });
        }
    },
/* ==========================================================================
   RENDER TEMPLATE
   ========================================================================== */
    render: function () {
    /* ===================
       Initial setup
       =================== */
        this.functions.initialSetup(this.settings);

    /* ===================
       Template context
       =================== */
        var context = {
            settings: this.settings
        }
        var html;
        var template = this.template;
        $(this.settings.placeholder).each( function (index, Element) {
            context.index = index;
            html = nunjucks.renderString(template, context);
            $(this).find('td:first').append(html);
        });
    },

/* ==========================================================================
   EVENTS
   ========================================================================== */
    events: function () {
        // Add click handler for Add button
        $('.dis-new-input').click(function () {
            $($(this).attr('data-trigger')).attr('checked', true );
            $('#Update').click();
        });
    },
/* ==========================================================================
   DESTROY
   ========================================================================== */
    destroy: function () {},
/* ==========================================================================
   BEFORE
   ========================================================================== */
    before: function () {},
/* ==========================================================================
   CALLBACK
   ========================================================================== */
    callback: function () {},

/* ==========================================================================
   INITIALIZATION
   ========================================================================== */
    initialize: function (options) {
        if ( (typeof __order_widgets == 'undefined') || (typeof __order_widgets.ADD_LABEL == 'undefined') ) return false;
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}