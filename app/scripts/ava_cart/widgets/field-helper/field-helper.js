/**
 * Field Helper
 *
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 */
AvaCart.Widgets.fieldHelper = {
/* ==========================================================================
   SETTINGS
   ========================================================================== */
    settings: {
        placeholder     : null,
        
        iconClass       : 'icon-info-circled',
        contentTypeHtml : false,
        contentImageSrc : null, // for contentTypeHtml = false
        contentHtml     : null, // for contentTypeHtml = true
        mirrored        : false,
        
        widgetClass     : null
    },
    template: null,

/* ==========================================================================
   FUNCTIONS
   ========================================================================== */
    functions: {},
/* ==========================================================================
   RENDER TEMPLATE
   ========================================================================== */
    render: function () {
    /* ===================
       Template context
       =================== */
        var context = {
            settings: this.settings
        }
        
        $(this.settings.placeholder).children().eq(0).wrap('<div class="field-wrapper"></div>');
        
        var html = nunjucks.renderString(this.template, context);
        $(this.settings.placeholder + ' .field-wrapper').append(html);
                
//        $(this.settings.placeholder).parent('.field-wrapper').find('.order__field__error').appendTo($(this.settings.placeholder).parent('.field-wrapper'));
    },

/* ==========================================================================
   EVENTS
   ========================================================================== */
    events: function () {},
/* ==========================================================================
   DESTROY
   ========================================================================== */
    destroy: function () {},
/* ==========================================================================
   BEFORE
   ========================================================================== */
    before: function () {},
/* ==========================================================================
   CALLBACK
   ========================================================================== */
    callback: function () {},

/* ==========================================================================
   INITIALIZATION
   ========================================================================== */
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}