/**
 * Quantity Field
 *
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 */
AvaCart.Widgets.quantityField = {
    timer: null,
    /* ==========================================================================
    SETTINGS
    ========================================================================== */
    settings: {
        placeholder: 'td.order__listing__item__qty'
    },
    template: null,

    /* ==========================================================================
    FUNCTIONS
    ========================================================================== */
    functions: {
        setQuantityValue: function($input, val, self) {
            $input.val(val);

            $('#Update').trigger('click');
        }
    },
    /* ==========================================================================
    RENDER TEMPLATE
    ========================================================================== */
    render: function() {
        /* ===================
        Template context
        =================== */
        var context = {
            settings: this.settings
        }
        var html = nunjucks.renderString(this.template, context);
        $(this.settings.placeholder).each(function(index, Element) {
            var $this = $(this);
            if ($this.find('[name^="qty"]:visible').length > 0) {
                $this.append(html);
                $this.find('.quantity-field__input').append($this.find('[name^="qty"]'));
            }
        });
    },

    /* ==========================================================================
    EVENTS
    ========================================================================== */
    events: function() {
        var self = this;
        
        $('.minus').click(function() {
            var $qty = $(this).parents('table.quantity-field:first').find('[name^="qty"]');
            var val = $qty.val();
            if (val > 1) {
                val -= 1;
                self.functions.setQuantityValue($qty, val);
            }
        });
        $('.plus').click(function() {
            var $qty = $(this).parents('table.quantity-field:first').find('[name^="qty"]');
            var val = parseInt($qty.val());
            if (val > 0 && val < 9999) {
                val++;
                self.functions.setQuantityValue($qty, val);
            }
        });
        $('[name^="qty"]').blur(function() {
            self.functions.setQuantityValue($(this), this.value);
        }).keyup(function() {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        });
    },
    /* ==========================================================================
    DESTROY
    ========================================================================== */
    destroy: function() {
        //$(this.settings.placeholder).html('');
        $('[name^="qty"]').each(function() {
            $(this).parents(this.settings.placeholder).append($(this));
        });
        $('.quantity-field').remove();
    },
    /* ==========================================================================
    BEFORE
    ========================================================================== */
    before: function() {},
    /* ==========================================================================
    CALLBACK
    ========================================================================== */
    callback: function() {},

    /* ==========================================================================
    INITIALIZATION
    ========================================================================== */
    initialize: function(options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}