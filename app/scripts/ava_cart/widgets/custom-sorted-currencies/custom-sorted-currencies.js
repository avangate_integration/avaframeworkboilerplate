/**
 * Custom Sorted Currencies
 *
 * @version 1.0.0
 * @since 1.0.0
 *
 * @param {object} options
 */
AvaCart.Widgets.customSortedCurrencies = function (options) {
    try {
        var settings = $.extend({
            removeIsoCode: false,
            topMethodsFirst: true,
            addSymbol: true,
            topCurrencies: ['EUR', 'USD', 'RON', 'GBP', 'TRY'],
            currencySymbols: {
                'EUR': '&euro;',
                'USD': '$',
                'GBP': '&pound;',
                'TRY': '&#8378;'
            }
        }, options);

        var currencySelector = $('[name="display_currency"]').eq(0);
        var currencyOptions = $('option', currencySelector);
        var selectedOptionVal = currencySelector.find(':selected').val();

        if (settings.removeIsoCode) {
            currencyOptions.each(function (i) {
                var currentOption = currencyOptions.eq(i);
                var newText = $.trim(currentOption.text().split('-')[1]);
                currencyOptions.eq(i).text(newText);
            });
        }

        currencyOptions.sort(function (a, b) {
            var t1 = $.trim(a.text.split('-')[1]);
            var t2 = $.trim(b.text.split('-')[1]);
            if (t1 > t2) {
                return 1;
            } else if (t1 < t2) {
                return -1;
            } else {
                return 0;
            }

        });

        if (settings.topMethodsFirst) {
            var firstOptions = $();
            var currentOption;
            var maxString = 0;
            var firstOption = '';
            currencyOptions.each(function () {
                currentOption = $(this);
                var v = currentOption.val();

                if (v == selectedOptionVal) {
                    currentOption.attr('selected', 'selected');
                }

                if ($.inArray(v, settings.topCurrencies) > -1) {
                    firstOptions = firstOptions.add(currentOption);
                    currencyOptions = currencyOptions.not(currentOption);
                } else if (v == selectedOptionVal) {
                    firstOption = currentOption;

                    currencyOptions = currencyOptions.not(currentOption);
                }
                if (maxString < currentOption.text().length) {
                    maxString = currentOption.text().length;
                }

            });
            firstOptions = firstOptions.add($('<option disabled="disabled">' + '-'.repeat(maxString / 2) + '</option>'));

        }

        currencySelector.empty().append(firstOptions).append(currencyOptions);
        if (firstOption != '') {
            currencySelector.prepend(firstOption);
        }

        if (settings.addSymbol) {
            $('option', currencySelector).each(function () {
                var $this = $(this);
                var t = t = $this.text();
                if (typeof settings.currencySymbols[$this.val()] != 'undefined') {
                    $this.html(settings.currencySymbols[$this.val()] + ' ' + t);
                }
            });
        }
        $("[name=display_currency] option[value='" + selectedOptionVal + "']").attr('selected', 'selected');
    } catch (error) {
        avaLog(error);
    }
}