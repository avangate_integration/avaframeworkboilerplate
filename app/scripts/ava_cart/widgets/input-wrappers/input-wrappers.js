/**
 * Input Wrappers
 *
 * @author Alexandru Salajan (alexandru.salajan@avangate.com)
 */
AvaCart.Widgets.inputWrappers = {
    settings: {},
    template: null,
    functions: {
        inputsWithTooltips: function (settings) {
            $('#order__checkout__billing__payoptions__table .order__text__field + .tooltip').each(function() {
                var inputContainer = $(this).parent();
                var inputField = inputContainer.children('.order__text__field');
                var inputTooltip = inputContainer.children('.tooltip');
                $('<div class="field-wrapper"></div>').append(inputField).append(inputTooltip).prependTo(inputContainer);
            });
        }
    },
    render: function () {
        this.functions.inputsWithTooltips(this.settings);
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
