/**
 * Remove Products
 *
 * @type {Object}
 */
AvaCart.Widgets.removeProducts = {
    settings: {
        view: 'img'
    },
    template: {},
    render: function () {
        var self = this;
        $('.order__listing__item__remove input[type="checkbox"]').parent('td.order__listing__item__remove').each(function () {
            var $this = $(this);
            $this.append('<div class="remove-product"><img src="'+ SPACER_IMAGE +'" class="remove-product-button" data-remove="'+ $this.find('[type="checkbox"]').hide().attr('name') +'"/></div>');
        });
    },
    events: function () {
        $('.remove-product-button').click(function () {
            $('[name="' + $(this).data('remove') + '"]').attr('checked', true);
            $('#Update').trigger('click');
        });
    },
    destroy: function () {
        $('.order__listing__item__remove input[type="checkbox"]').show();
        $('.remove-product').remove();
    },
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}