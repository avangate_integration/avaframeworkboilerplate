/**
 * Validate Mandatory Fields
 *
 * @version 1.0.0
 * @type    {Object}
 */
AvaCart.Widgets.validateMandatoryFields = {
    settings: {
        selector: '.order__text__field__mandatory, .order__select__field__mandatory'
    },
    render: function () {},
    functions: {
        validateFields: function ($el) {
            if ($.trim($el.val()) != '' && $el.attr('class').search('error') == -1) {
                $el.addClass('valid');
            } else {
                $el.removeClass('valid')
            }
        }
    },
    events: function () {
        var self = this;
        $(self.settings.selector).blur(function () {
            var $this = $(this);
             if ($this.attr('id') == 'fullname') {
                if ($.trim($this.val()).split(' ').length < 2) {
                    $this.addClass('error order__text__field__error');
                } else {
                    $this.removeClass('error order__text__field__error');
                }
            }
            if ($this.parents('.select-box:first').length) {
                if ($this.attr('class').search('error') == -1) {
                    $this.parents('.select-box:first').addClass('valid-select');
                } else {
                    $this.parents('.select-box:first').removeClass('valid-select');
                }
            }
            self.functions.validateFields($this);
        });

        $('.order__select__field__mandatory').blur(function () {
            var $this = $(this);
        });
    },
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
