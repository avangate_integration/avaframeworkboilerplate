/**
 * Up Selling
 *
 * @author  Alexandru Salajan (alexandru.salajan@avangate.com)
 * @type  {Object}
 */
AvaCart.Widgets.upSelling = {
/* ==========================================================================
   SETTINGS
   ========================================================================== */
    settings: {
        showPriceBeforeDescription: false,
        hidePriceLabel: false,
    },

/* ==========================================================================
   FUNCTIONS
   ========================================================================== */
    functions: {},
/* ==========================================================================
   RENDER TEMPLATE
   ========================================================================== */
    render: function () {
        if (this.settings.showPriceBeforeDescription) {
            $('#order__upsell__custom__description').insertAfter('.order__upsell__new__price__total__wrap');
        }
        if (this.settings.hidePriceLabel) {
            $('.order__upsell__new__price__total__label').hide();
        }
    },

/* ==========================================================================
   EVENTS
   ========================================================================== */
    events: function () {},
/* ==========================================================================
   DESTROY
   ========================================================================== */
    destroy: function () {},
/* ==========================================================================
   BEFORE
   ========================================================================== */
    before: function () {},
/* ==========================================================================
   CALLBACK
   ========================================================================== */
    callback: function () {},

/* ==========================================================================
   INITIALIZATION
   ========================================================================== */
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}