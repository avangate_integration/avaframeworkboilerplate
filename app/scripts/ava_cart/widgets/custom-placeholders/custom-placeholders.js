/**
 * Custom Placeholders
 *
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 */
AvaCart.Widgets.customPlaceholders = {
/* ==========================================================================
   SETTINGS
   ========================================================================== */
    settings: {
        placeholderSupport  : true
    },
    template: null,

/* ==========================================================================
   FUNCTIONS
   ========================================================================== */
    functions: {
        setPlaceholderAttributes: function () {
            var customPlaceHolderText;
            
            // Bank Issuer
            $('#bank_issuer option').eq(0).text($('#data__bank_issuer .order__checkout__form__label').text().replace(':', ''));
            
            $('li:not(#order__cart__contents, #order__additional__fields)').find('input.order__text__field').each(function () {
                $(this).parent().prev().hide();
                $('li:not(#order__additional__fields)').find('span.order__text__label').parent('td').hide();

                // Grabbing custom PlaceHolder text.
                var fieldContainer = $(this).parent();
                if (fieldContainer.hasClass('field-wrapper')) {
                    fieldContainer = fieldContainer.parent();
                }
                if ($(this).attr('id') == 'company') {
                    customPlaceHolderText = fieldContainer.prev().find('span#comp_optional').text().replace(':', '');
                } else {
                    var mandatoryField = $('#' + fieldContainer.prev().find('span.order__text__label:eq(0)').attr('id') + '__mandatory');
                    customPlaceHolderText = fieldContainer.prev().find('span.order__text__label:eq(0)').text().replace(':', '');
                    if (mandatoryField.css('display') == 'none')
                        customPlaceHolderText = customPlaceHolderText.replace(mandatoryField.text(), '');
                }

                if (customPlaceHolderText.length) {
                    $(this).attr('placeholder', $.trim(customPlaceHolderText));
                }

            });
            
            $('li#order__additional__fields').find('input.order__text__field').each(function () {
                $(this).parent().prev().hide();
//                $('li#order__additional__fields span.order__text__label').hide();

                // Grabbing custom PlaceHolder text.
                var mandatoryField = $('#' + $(this).parent().prev().find('span.order__text__label:eq(0)').attr('id') + '__mandatory');
                customPlaceHolderText = $(this).parent().parent().find('span.order__text__label:eq(0)').text().replace(':', '');
                if (mandatoryField.css('display') == 'none')
                    customPlaceHolderText = customPlaceHolderText.replace(mandatoryField.text(), '');

                if (customPlaceHolderText.length) {
                    $(this).attr('placeholder', $.trim(customPlaceHolderText));
                }

            });
        },
        placeholderFix: function () {
            $(function () {
                $(':input[placeholder]').placeholder();
            });
            $('.custom-placeholder').each(function () {
                $(this).css('left', $(this).prev('input').css('padding-left'));
                $(this).css('top', $(this).prev('input').css('padding-top'))
            });
            return false;
        }
    },
/* ==========================================================================
   RENDER TEMPLATE
   ========================================================================== */
    render: function () {
        $('body').addClass('custom-placeholders');
        $('#order__checkout__billing__info__table td.note').prev().hide();
        
        this.functions.setPlaceholderAttributes();
        
        if (!this.settings.placeholderSupport) {
            this.functions.placeholderFix();
        }
    },

/* ==========================================================================
   EVENTS
   ========================================================================== */
    events: function () {},
/* ==========================================================================
   DESTROY
   ========================================================================== */
    destroy: function () {},
/* ==========================================================================
   BEFORE
   ========================================================================== */
    before: function () {},
/* ==========================================================================
   CALLBACK
   ========================================================================== */
    callback: function () {},

/* ==========================================================================
   INITIALIZATION
   ========================================================================== */
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}