/**
 * Secure Checkut
 *
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 */
AvaCart.Widgets.secureCheckout = {
/* ==========================================================================
   SETTINGS
   ========================================================================== */
    settings: {
        placeholder         : null,

        headerLabel         : null,
        headerIconClass     : 'icon-lock',
        showBox             : false,
        showBoxTitle        : false,
        showTitle           : true,

        avangateLogoSrc     : null,
        content             : null,
        showSealsAfterText  : false,

        widgetClass         : null
    },
    template: null,

/* ==========================================================================
   FUNCTIONS
   ========================================================================== */
    functions: {},
/* ==========================================================================
   RENDER TEMPLATE
   ========================================================================== */
    render: function () {
    /* ===================
       Template context
       =================== */
        var context = {
            settings: this.settings
        }
        
        var html = nunjucks.renderString(this.template, context);
        $(this.settings.placeholder).append(html);
        $('.secure-checkout-seals').append($('a[href*="mcafeesecure.com"]')).append($('#order__secure__seal a'));
    },

/* ==========================================================================
   EVENTS
   ========================================================================== */
    events: function () {},
/* ==========================================================================
   DESTROY
   ========================================================================== */
    destroy: function () {},
/* ==========================================================================
   BEFORE
   ========================================================================== */
    before: function () {},
/* ==========================================================================
   CALLBACK
   ========================================================================== */
    callback: function () {},

/* ==========================================================================
   INITIALIZATION
   ========================================================================== */
    initialize: function (options) {
        if (typeof __order_widgets == 'undefined') return false;
        if (typeof __order_widgets.SECURE_CHECKOUT == 'undefined') return false;
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}