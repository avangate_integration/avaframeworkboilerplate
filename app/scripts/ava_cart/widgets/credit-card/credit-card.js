/**
 * Credit Card
 *
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 */
AvaCart.Widgets.creditCard = {
/* ==========================================================================
   SETTINGS
   ========================================================================== */
    settings: {
        placeholder     : null,
        
        showSelectBoxes : false,        
        width           : 350
    },
    template: null,

/* ==========================================================================
   FUNCTIONS
   ========================================================================== */
    functions: {
        checkForErros: function () {
            // Check for if the expiration date field has errors
            try {
                if ( $('#cbExpMounth').hasClass('order__select__field__error') || $('#cbExpYear').hasClass('order__select__field__error') ) {
                    $('#cardExpiry').addClass('field-error order__text__field__error');
                }
            } catch (err) {}
        }
    },
/* ==========================================================================
   RENDER TEMPLATE
   ========================================================================== */
    render: function () {
        var cardDataInputPlaceholder = 'MM / YYYY';
        if ( $('#card__data__expdates .order__checkout__form__input').length > 0) {
            if (!this.settings.showSelectBoxes) {
                $('#card__data__expdates .order__checkout__form__input > *').hide();
                $('#card__data__expdates .order__checkout__form__input').append('<input name="cardExpiry" type="text" id="cardExpiry" class="card-expiry order__text__field order__text__field__mandatory  payment-content__input text field-mandatory" placeholder="' + cardDataInputPlaceholder + '">');
            }
        } else if ( $('#card__data__expdates .order__checkout__form__input__error').length > 0) {
            $('#card__data__expdates .order__checkout__form__input__error > *').hide();
            $('#card__data__expdates .order__checkout__form__input__error').append('<input name="cardExpiry" type="text" id="cardExpiry" class="card-expiry order__text__field order__text__field__mandatory  payment-content__input text field-mandatory" placeholder="' + cardDataInputPlaceholder + '">');
        }

        $('form#frmCheckout').card({
            // a selector or DOM element for the container
            // where you want the card to appear
            container: this.settings.placeholder, // *required*

            formSelectors: {
                numberInput: 'input#tiCNumber', // optional — default input[name="number"]
                expiryInput: 'input#cardExpiry', // optional — default input[name="expiry"]
                cvcInput: 'input#tiCVV', // optional — default input[name="cvc"]
                nameInput: 'input#nameoncard' // optional - defaults input[name="name"]
            },
            
            width: this.settings.width,
            
            messages: {
//                    validDate: 'valid\ndate', // optional - default 'valid\nthru'
//                    monthYear: 'mm/yyyy', // optional - default 'month/year'
            }
        });

        // Check for if the expiration date field has errors
        this.functions.checkForErros();
    },

/* ==========================================================================
   EVENTS
   ========================================================================== */
    events: function () {
        if (this.settings.showSelectBoxes) {
            $('#cbExpMounth, #cbExpYear').change(function (event) {
                $('.jp-card-expiry').text($('#cbExpMounth').val() + '/' + $('#cbExpYear').val());
            });
        } else {
            $('#cardExpiry').change(function (event) {
                var expirationDate = $(this).val().split(' / ');
                $('#cbExpMounth').val(expirationDate[0]).change();
                $('#cbExpYear').val(expirationDate[1]).change();
            });
        }
        
        /* ADRIAN CODE: *//*.keyup(function (event) {
            var monthInput = this.value.split(' / ')[0];
            var yearInput = this.value.split(' / ')[1]
            
            if (monthInput
            
            
            if ($('#cbExpYear option:eq(1)') < yearInput) {
                this.value = this.value.split(' / ')[0]
            }
                
            
             var $('#cbExpYear option:last').val(expirationDate[1]).change();
            
            
            this.value = '12 / 2015';
            return;
//            var $('#cbExpYear').val(expirationDate[1]).change();
//            var expirationDate = $(this).val().split(' / ');
//            var month = expirationDate[0];
//            var year = expirationDate[1];
//            year = 2012;
//            $('#cardExpiry').val(month + ' / ' + year);
        });*/
        
        // TODO (ALEX):
//        $('#cardExpiry').keyup(function (event) {
//            var expirationDate = $(this).val().split(' / ');
//            var month = expirationDate[0];
//            var year = expirationDate[1];
//            year = 2012;
//            $('#cardExpiry').val(month + ' / ' + year);
//        });

        var self = this;
        $(document).ajaxSuccess(function() {
            self.functions.checkForErros();
        });
    },
/* ==========================================================================
   DESTROY
   ========================================================================== */
    destroy: function () {},
/* ==========================================================================
   BEFORE
   ========================================================================== */
    before: function () {},
/* ==========================================================================
   CALLBACK
   ========================================================================== */
    callback: function () {},

/* ==========================================================================
   INITIALIZATION
   ========================================================================== */
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
