/**
 * Hotline
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 */
AvaCart.Widgets.hotline = {
    settings: {
        placeholder: null,
        HOTLINE: null,
        SUPPORT: null,
        phone: null, // E.g. { country: Test, number: +123546547 };
        country: null,
        
        showLanguageInHeader: true,
                
        widgetClass: null
    },
    template: null,
    render: function () {
        var context = {
            settings: this.settings
        }
        
        var html = nunjucks.renderString(this.template, context);
        $(this.settings.placeholder).append(html);
        
        // Add language to Header
        if (this.settings.showLanguageInHeader) {
            $('#order__header__languages').clone(true)
                .removeAttr('id')
                .removeAttr('class')
                .addClass('hotline-language-content')
                .appendTo($(this.settings.placeholder).find('.hotline-language'));
            $(this.settings.placeholder).find('.language__label').removeAttr('id');
            $('.hotline-language-content').find('select').removeAttr('onchange').change(function (event) {
                $('#order__header__languages select').val($(this).val());
                $('#action').val('language');
                $('#order__container form').submit();
            })
        }
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            if (noFooter) return false;
            
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
