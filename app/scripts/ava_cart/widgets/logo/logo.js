/**
 * Logo
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 */
AvaCart.Widgets.logo = {
    settings: {
        placeholder:null,
        link: null,
        image: null,
        text: null
    },
    template: null,
    render: function () {
        if (!this.settings.placeholder) {
            avaLog(this, 'Placeholder Missing!')
        }
        $(this.settings.placeholder).append(nunjucks.renderString(this.template, this.settings));
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
