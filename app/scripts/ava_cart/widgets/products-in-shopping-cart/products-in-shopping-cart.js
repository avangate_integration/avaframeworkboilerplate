/**
 * Products in Sopping Cart
 *
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 */
AvaCart.Widgets.productsInShoppingCart = {
/* ==========================================================================
   SETTINGS
   ========================================================================== */
    settings: {
        placeholder             : '#order__cart__contents',
        
        hideVATtext             : true,
        showCurrencyInHeader    : false,
        showCurrencyInFooter    : false,
        
        showProductImage        : false,
        productImageSize        : '60',
        showRemoveIconFirst     : false,
        
        layout                  : 'layout-1', // AVAILABLE OPTIONS: [default, layout-1, layout-2],
        widgetClass             : null
    },
    template: null,

/* ==========================================================================
   FUNCTIONS
   ========================================================================== */
    functions: {
        formatTotals: function () {
            try {
                $(".products-totals .products-total-right .products-total-row").each(function () {
                    if ($(this).text() != '') {
                        var text_price, text_price_value, new_text_price;
                        text_price = $(this).text().split(':');
                        text_price_value = text_price[1].split('/');
                        if (text_price_value.length == 2) {
                            new_text_price = '<span class="prod-total-text">' + text_price[0] + ': </span><span class="order__display__total">' + text_price_value[0] + '/</span> <span class="order__billing__total">' + text_price_value[1] + '</span>';
                        } else {
                            new_text_price = '<span class="prod-total-text">' + text_price[0] + ': </span> <span class="order__billing__total">' + text_price_value[0] + '</span>';
                        }
                        $(this).html(new_text_price);
                    }
                });
            } catch (error) {
                avaLog(error);
            }
        },
        
        addProductContent: function (settings) {
            try {
                if (settings.showProductImage) {
                    $('#order__products .order__listing__header').prepend($('<td class="order__listing__header__image"></td>'));
                }
                if (settings.showRemoveIconFirst) {
                    $('#order__products .order__listing__header').prepend($('#order__products .order__listing__header__remove'));
                    $('.order__product__discount .order__listing__item__remove').hide();
                    $('.order__product__discount .order__listing__item__total__price').attr('colspan','2');
                }
                var nrOfProductsWithImage = 0;
                $("#order__products .order__listing__row:not(.order__product__discount)").each(function (index, Element) {
                    if (settings.showProductImage) {
                        var productID = $(Element).find('.order__listing__item__remove input').eq(0).val();
                        var productImageSrc = '';
                        $(Element).prepend('<td class="order__listing__item__image"></td>');
                        for (var i=0; i<omniture_vars.CART_PRODUCTS.length;i++) {
                            if ( omniture_vars.CART_PRODUCTS && omniture_vars.CART_PRODUCTS[i] && omniture_vars.CART_PRODUCTS[i].ProductID ) {
                                if (omniture_vars.CART_PRODUCTS[i].ProductID === productID) {
                                    if (omniture_vars.CART_PRODUCTS[i].hasOwnProperty('ProductImages')) {
                                        if (omniture_vars.CART_PRODUCTS[i].ProductImages[settings.productImageSize] != 'undefined') {
                                            productImageSrc = omniture_vars.CART_PRODUCTS[i].ProductImages[settings.productImageSize];
                                            $(Element).find('.order__listing__item__image').html('<img alt="" src="' + productImageSrc + '" />');
                                            nrOfProductsWithImage++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (settings.showRemoveIconFirst) {
                        $(Element).prepend($(Element).find('.order__listing__item__remove'));
                    }
                });
                if (settings.showProductImage && (nrOfProductsWithImage === 0)) { // if there are no images for products, do not show Image column
                    $('.order__listing__header__image, .order__listing__item__image').hide();
                }
                if (settings.showProductImage && (nrOfProductsWithImage > 0)) { // if there are no images for products, do not show Image column
                    $('.order__product__discount .order__listing__item__name').attr('colspan','2');
                }
            } catch (error) {
                avaLog(error);
            }
        }
    },

/* ==========================================================================
   RENDER TEMPLATE
   ========================================================================== */
    render: function () {
        this.functions.formatTotals();
        if (this.settings.showProductImage || this.settings.showRemoveIconFirst) {
            this.functions.addProductContent(this.settings);
        }
        
        if (this.settings.hideVATtext) {
            $('.products-content__vat').parent('td').parent('tr').hide();
        }
        
        if (this.settings.showCurrencyInHeader) {
            var headerText = $('.products__title').text();
            $('.products__title').addClass('row').text('').prepend('<div class="products__title__text col-sm-6">' + headerText + '</div>');
            $('#order__header__currencies').clone(true)
                .removeAttr('id')
                .removeAttr('class')
                .addClass('products-header-currency col-sm-6')
                .appendTo('.products__title');
            $('.products-header-currency').find('.currency__label').removeAttr('id');
        }
        
        if (this.settings.showCurrencyInFooter) {
            $('#order__header__currencies').clone(true)
                    .removeAttr('id')
                    .removeAttr('class')
                    .addClass('order__checkout__form__billing__currencies currency')
//                    .appendTo('.order__box__footer .order__box__aux2');
                    .appendTo('#order__cart__contents .products__content');
        }
        
        switch(this.settings.layout) {
            // DEFAULT
            case 'default':
                $('#coupon-wrap').append($('#order__coupon__input, .coupon-placeholder, #order__apply__discount, #order__update__cart__icon'));
                $('#coupon-wrap > input').hide();
                $('#order__update__cart__icon').hide();
                $('#coupon-wrap').html($('#coupon-wrap').html().replace(/&nbsp;/gi,''));
                
                $('#order__sub__total__row').after($('#order__coupon__input').parent('td').parent('tr'));
                break;
            // LAYOUT 1 => SDL
            case 'layout-1':

                break;
            // LAYOUT 2 => HP
            case 'layout-2':
                // Add classes
                $('#order__totals #order__coupon__input').parent('td').parent('tr').addClass('coupon-container');
                $('#order__totals table.order__gift__option').parent('td').parent('tr').addClass('order-gift-option-container');
                $('#order__totals table.order__dis__option').parent('td').parent('tr').addClass('order-dis-option-container');
                $('#order__totals table.order__backupcd__option').parent('td').parent('tr').addClass('order-backupcd-option-container');
                // Reorder elements
                $('#order__totals .order__checkout__summary').append($('.order-dis-option-container'));
                $('#order__totals .order__checkout__summary').append($('.order-backupcd-option-container'));
                break;
            // LAYOUT 3
            case 'layout-3':
                var context = {
                    settings: this.settings
                }
                var html = nunjucks.renderString(this.template, context);
                
                $('#order__sub__total__row').after($('#order__coupon__input').parent('td').parent('tr'));
                $('#order__products').prepend(html);
                
                if ($('.order__cart__products__table').length > 0) {
                    $('.order__cart__products__table__wrapper').append($('.order__cart__products__table'));
                } else if ($('.order__checkout__products__table').length > 0) {
                    $('.order__cart__products__table__wrapper').append($('.order__checkout__products__table'));
                }
                
                $('.order__cart__totals__table').append($('.products-billing-txt').parent('td').addClass('products-billing-txt-wrapper').parent('tr'));
                $('.order__cart__totals__table').append($('#order__sub__total__row'));
                
                $('.order__cart__updates__table__wrapper').append($('#order__totals'));
                
                $('.order__cart__vat__table').append($('.products-content__vat').parent('td').parent('tr'));
                break;
            // LAYOUT 4
            case 'layout-4':
                $('#order__totals').after('<table class="products-in-cart-additional-options-wrapper"><tbody class="products-in-cart-additional-options"></tbody></table>');
                $('.products-in-cart-additional-options-wrapper').append($('#order__coupon__input').parent('td').parent('tr'));
                $('.products-in-cart-additional-options-wrapper').append($('.order__dis__option').parent('td').parent('tr'));
                $('.products-in-cart-additional-options-wrapper').append($('.order__backupcd__option').parent('td').parent('tr'));
                $('.products-in-cart-additional-options-wrapper').append($('.order__gift__option').parent('td').parent('tr'));
                $('.products-in-cart-additional-options-wrapper').append($('.products-content__vat').parent('td').parent('tr'));
                
                break;
        }

        /*https://redmine.avangate.com/issues/15798*/

        $('.sprite.order__dis__icon').after('<span class="icon icon-cloud-download"></span>');
        $('.order__backupcd__icon.sprite').after('<span class="icon icon-cd"></span>');

        $(this.settings.placeholder).addClass(this.settings.layout + ' ' + this.settings.widgetClass);
    },

/* ==========================================================================
   EVENTS
   ========================================================================== */
    events: function () {},
/* ==========================================================================
   DESTROY
   ========================================================================== */
    destroy: function () {},
/* ==========================================================================
   BEFORE
   ========================================================================== */
    before: function () {},
/* ==========================================================================
   CALLBACK
   ========================================================================== */
    callback: function () {},

/* ==========================================================================
   INITIALIZATION
   ========================================================================== */
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
