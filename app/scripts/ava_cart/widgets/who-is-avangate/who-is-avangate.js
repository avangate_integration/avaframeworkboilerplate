/**
 * Who is Avangate
 *
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 */
AvaCart.Widgets.whoisAvangate = {
/* ==========================================================================
   SETTINGS
   ========================================================================== */
    settings: {
        placeholder: null,
        
        orderProcessedBy: null,
        
        avangateLogoSrc : 'https://secure.avangate.com/images/merchant/1ae6464c6b5d51b363d7d96f97132c75/avangate_new_logo.png',
        vendorLogoSrc   : '',
        
        widgetClass     : null
    },
    template: null,

/* ==========================================================================
   FUNCTIONS
   ========================================================================== */
    functions: {
        buildPopup: function () {
            // Build the popup
            $('#wia-popup').dialog({
                bgiframe: true,
                width: 580,
                modal: true,
                position: ['center', 150],
                autoOpen: false,
                dialogClass: 'wia-popup'
            });
        }
    },
/* ==========================================================================
   RENDER TEMPLATE
   ========================================================================== */
    render: function () {
    /* ===================
       Template context
       =================== */
        var context = {
            settings: this.settings
        }
        var html = nunjucks.renderString(this.template, context);
        $(this.settings.placeholder).append(html);

    /* ===================
       Build the popup
       =================== */
        this.functions.buildPopup();
    },

/* ==========================================================================
   EVENTS
   ========================================================================== */
    events: function () {
        $('#wia-text .link').click(function (event) {
            $('#wia-popup').dialog('open');
            event.preventDefault();
        })
    },
/* ==========================================================================
   DESTROY
   ========================================================================== */
    destroy: function () {},
/* ==========================================================================
   BEFORE
   ========================================================================== */
    before: function () {},
/* ==========================================================================
   CALLBACK
   ========================================================================== */
    callback: function () {},

/* ==========================================================================
   INITIALIZATION
   ========================================================================== */
    initialize: function (options) {
        if (typeof __order_processed_by == 'undefined') return false;
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}