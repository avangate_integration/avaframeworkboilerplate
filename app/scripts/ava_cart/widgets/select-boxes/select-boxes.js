/**
 * Select Boxes
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @version 1.0.0
 */

AvaCart.Widgets.selectBoxes = {
    settings: {
        defaultWidth: '205px',
        selector: 'select:not(#Promotion_Methods)'
    },
    resetSelectBoxesSelectedValues: function () {
        $('.select-box').each(function () {
            var $this = $(this);
            $this.removeClass('order__select__focus').find('.selected-option').html($this.find('option:selected').text());
            if ($this.find('select').val()=='') { $this.removeClass('valid-select'); }
        });
    },
    render: function () {
        var self = this;
        $(this.settings.selector).each(function () {
            var $this = $(this),
                id = ($this.attr('id') ? $this.attr('id') : avaSlugify($this.attr('name'))).replace(/\[/g,'').replace(/\]/g,'') + '-select-box';
                value = $.trim($this.find('option:selected').text());
                style = 'width:' + (this.offsetWidth ? this.offsetWidth + 'px' : self.settings.defaultWidth);
            $this.addClass('force-reset-select').wrap('<div class="select-box" style="'+ style +'" id="'+ id +'" />');
            console.log('');
            $('#' + id).prepend('<span class="selected-option">'+ value +'</span>');
            $('#' + id).prepend('<div class="select-box-arrow-wrapper"><span class="select-box-arrow"></span></div>');
        });
    },
    events: function () {
        var self = this;
        $(self.settings.selector).each(function () {
            var $this = $(this);

            if ($this.hasClass('order__select__field__error')) {
                $this.parent().addClass('order__select__field__error');
            } else {
                $this.parent().removeClass('order__select__field__error');
            }
            $this.focus(function () {
                $(this).parent().addClass('order__select__focus');
            }).blur(function () {
                var $this = $(this);
                $this.parent().removeClass('order__select__focus');
                if ($this.hasClass('order__select__field__error')) {
                    $this.parent().addClass('order__select__field__error');
                } else {
                    $this.parent().removeClass('order__select__field__error');
                }
            }).change(function () {
                var $this = $(this);
                $this.parent().find('.selected-option').html($this.find('option:selected').text());
                $this.parent().removeClass('order__select__focus');
                self.resetSelectBoxesSelectedValues();
            }).bind('keyup',function () {
                var $this = $(this);
                $this.parent().find('.selected-option').html($this.find('option:selected').text());
                $this.parent().removeClass('order__select__focus');
                self.resetSelectBoxesSelectedValues();
            });

        });
        $('.order__select__focus').click(function (event) {
            $(this).removeClass('order__select__focus');
        });

        // For Checkout Without Review add errors after validating ajax.
        if (avaPage.isCheckoutPage && avaPage.withoutReview) {
            $(document).ajaxSuccess(function () {
                $(self.settings.selector).each(function () {
                    var $this = $(this);
                    if ($this.hasClass('order__select__field__error')) {
                        $this.parent().addClass('order__select__field__error');
                    } else {
                        $this.parent().removeClass('order__select__field__error');
                    }
                    self.resetSelectBoxesSelectedValues();
                });
            })
        }
    },
    destroy: function () {
        $('.select-box').each(function () {
            var $this = $(this);
           $this.after($this.find('select').removeClass('force-reset-select'));
           $this.remove();
        });
    },
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
