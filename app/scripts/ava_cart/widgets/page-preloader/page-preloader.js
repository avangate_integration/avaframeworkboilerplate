/**
 * Hotline
 *
 * @author Alexandru Salajan (alexandru.salajan@avangate.com)
 */
AvaCart.Widgets.pagePreloader = {
    settings: {
        placeholder: null,
        duration: 400
    },
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(this.settings, options);
            
            this.before();
            $('#order__container').css('visibility', 'visible');
            $(this.settings.placeholder).fadeOut(this.settings.duration, this.callback);
        } catch (error) {
            avaLog(error);
        }
    }
}