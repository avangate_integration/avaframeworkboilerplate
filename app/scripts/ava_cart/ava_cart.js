/**
 * AvaCart
 *
 * @version  1.0.0
 * @type {Object}
 */
var AvaCart = {
	version: 1.0,
	Utils: {},
	Functions: {},
	PageStructure: {},
	Layouts: {},
	Widgets: {},
	Callbacks: {},
    Custom: {}
}

/**
 * Initialize
 *
 * Will iterate the callbacks object and try to callback fn
 * if condition is true or undefined;
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @version 1.0.0
 * @since   1.0.0
 */
AvaCart.initialize = function (args) {
	if ((typeof args == 'undefined' || !(args instanceof Array)) && !window.AvaCartInitialized) {
		return false;
	}
	for (i in args) {
		try {
			if (typeof this.Callbacks[args[i]] != 'undefined' && this.Callbacks[args[i]] instanceof Object) {
				if ((typeof this.Callbacks[args[i]].condition == 'undefined' || (typeof this.Callbacks[args[i]].condition != 'undefined' &&
						this.Callbacks[args[i]].condition === true)) && typeof this.Callbacks[args[i]].callback == 'function') {
					try {
						this.Callbacks[args[i]].callback();
					} catch (error) {
						avaLog(error);
					}
				}
			}
		} catch (error) {
			avaLog(error);
		}
	}
	window.AvaCartInitialized = true;
};
/**
 * @param  {String}
 * @param  {String}
 * @param  {String}
 * @return {String}
 */
AvaCart.Functions.getTranslation = function(__phrase, __def, __lang) {
	try {
		var omnitureLANG = (typeof omniture_vars !== 'undefined' && omniture_vars.hasOwnProperty('LANGUAGE')) ? omniture_vars.LANGUAGE.replace('-', '_') : '';
		if (typeof __lang !== 'undefined' && typeof __lang !== null)
			omnitureLANG = __lang.replace('-', '_');

		if (templateDictionary.hasOwnProperty(omnitureLANG) && templateDictionary[omnitureLANG].hasOwnProperty(__phrase)) {
			return templateDictionary[omnitureLANG][__phrase];
		} else {
			if (typeof __def !== 'undefined' && typeof __def !== null && __def != "")
				return __def;
			else if (templateDictionary.hasOwnProperty('en') && templateDictionary['en'].hasOwnProperty(__phrase) && (typeof __lang === 'undefined' || typeof __lang === null))
				return templateDictionary['en'][__phrase];
			else if ((typeof __lang !== 'undefined' || typeof __lang !== null) && templateDictionary.hasOwnProperty(__lang) && templateDictionary[__lang].hasOwnProperty(__phrase))
				return templateDictionary[__lang][__phrase];
			else
				return '';
		}
	} catch (error) {
		avaLog(error);
	}
};