/**
 * Layout 2
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 */
AvaCart.Layouts.layout2 = {
    settings: {},
    render: function () {
        $('body').addClass('layout-2');
        try {
            $('.tmp-li-fix').removeClass('tmp-li-fix');
            $('#order__container').addClass('col-md-12');
            if (avaPage.current == 'checkout') {
                $('#order__checkout__billing__data, li#order__cart__contents, #order__checkout__payoptions__data, #order__additional__fields').addClass('col-md-12');
                $('.order__checkout__button__container').parent('li').addClass('col-md-12');
            
                $('#order__finalTotalPrice').insertBefore($('.order__checkout__button__container'));
            } else if (avaPage.current == 'verify') {
                $('#frmFinish > div').addClass('col-md-12');
            }
            
            $('#order__finish__finish__order').addClass('col-md-12');
        } catch(error) {
            avaLog(error);
        }
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
