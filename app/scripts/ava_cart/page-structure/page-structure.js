/**
 * Page Structure
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 */
AvaCart.PageStructure = {
    template: null,
    render: function () {
        if (!this.template) {
            avaLog(this, 'No template to render!');
        }
        $('body div:first').before(this.template);
        var pageCode = document.getElementById('page-code');
        var orderContainer = document.getElementById("order__container") || document.getElementById("wrap-form") || document.getElementById("wrap-thankyou") ;
        pageCode.appendChild(orderContainer);
        $('#footer .container .row').append($('.assistance'));
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function () {
        try {
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
