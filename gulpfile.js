'use strict';

// Gulp & Tools We'll Use
var gulp = require('gulp'),
	plugins = require('gulp-load-plugins')(),
	del = require('del'),
	runSequence = require('run-sequence'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload,
	fs = require('fs'),
	htmlToJs = require('html-to-js'),
	jshint = require('gulp-jshint'),
	csslint = require('gulp-csslint'),
	recess = require('gulp-recess'),
	jscs = require('gulp-jscs'),
	read = fs.readFileSync,
	autoPrefixerBrowsers = [
      'ie >= 10',
      'ie_mob >= 10',
      'ff >= 30',
      'chrome >= 34',
      'safari >= 7',
      'opera >= 23',
      'ios >= 7',
      'android >= 4.4',
      'bb >= 10'
    ];

/**
 * Create Js variables from HTML templates.
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @version 1.0.0
 * @since   1.0.0
 * @usage [{'AvaCart.Widgets.PaymentWidget': 'app/scripts/widgets/payment-widget/custom-select.html'}]
 */
function html2js(tpls, dest) {
	var source = [];
	for (var i in tpls) { // iterate objects in array
		for (var j in tpls[i]) {
			var html = read(tpls[i][j], 'utf8'); // read file from path
			var js = htmlToJs(html);
			// push templates as variables in source array
			source.push(js.replace('module.exports', j + '.template'));
		}
	}
	// write template variables to js file
	fs.writeFile(dest, source.join(''), function (err) {
		if (err) {
			return console.log(err);
		}
		console.log('The file was saved!');
	});
}

// Clean output directory.
gulp.task('clean', del.bind(null, ['.tmp', 'dist', 'documentation']));

// Js Lint
gulp.task('jslint', function (cb) {
	return gulp.src([
	        'app/custom/scripts/scripts.js',
			'app/scripts/ava_cart/ava_cart_init.js'
	    ])
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish', {
			verbose: true
		}))
		.pipe(jshint.reporter('fail'))
		.pipe(plugins.notify({
			title: 'JSHint',
			message: 'JSHint Passed. Boss de boss!',
		}))
});

gulp.task('jscs', function () {
	gulp.src([
	        'app/custom/scripts/scripts.js',
			'app/scripts/ava_cart/ava_cart_init.js'
	    ])
		.pipe(jscs({
			fix: true
		}))
		.pipe(plugins.notify({
			title: 'JSCS',
			message: 'JSCS Passed. '
		}));
});

gulp.task('csslint', function () {
	return gulp.src('app/custom/styles/styles.less')
		.pipe(recess({
			// Default Options
			strictPropertyOrder: true, // Complains if not strict property order.
			noIDs: true, // Doesn't complain about using IDs in your stylesheets.
			noJSPrefix: true, // Doesn't complain about styling .js- prefixed classnames.
			noOverqualifying: true, // Doesn't complain about overqualified selectors (ie: div#foo.bar).
			noUnderscores: true, // Doesn't complain about using underscores in your class names.
			noUniversalSelectors: true, // Doesn't complain about using the universal * selector.
			zeroUnits: true, // Doesn't complain if you add units to values of 0.
		}))
		.pipe(recess.reporter())
});

// Compile and Automatically Prefix Stylesheets.
gulp.task('styles', function () {
	return gulp.src([
//        'app/styles/ava_icons_font.less',
        'app/styles/ava_cart.less'
      ])
		//.pipe(plugins.changed('styles', {
		//     extension: '.less'
		// }))
		.on('error', plugins.util.log)
		.pipe(plugins.less({
			// paths: [ path.join(__dirname, 'less', 'includes') ]
		}))
		.on('error', plugins.util.log)
		//        .on('error', console.error.bind(console))
		//.pipe(plugins.autoprefixer({
		//    browsers: autoPrefixerBrowsers
		//}))
		.on('error', plugins.util.log)
		.pipe(gulp.dest('.tmp/styles'))
		// Concatenate And Minify Styles
		//.pipe(plugins.
		// if ('*.css', plugins.csso()))
		// .pipe(plugins.concat('main.css'))
		.pipe(gulp.dest('dist/styles'))
		// .pipe(plugins.size({
		//      title: 'styles'
		//}));
});

// Uglify & concatenate ava_dependencies scripts.
gulp.task('ava_legacy', function () {
	gulp.src([
        'app/scripts/ava_legacy.js'
    ])
		.pipe(plugins.concat('ava_legacy.js'))
		.pipe(gulp.dest('dist/scripts/'))
});

// Uglify & concatenate ava_dependencies scripts.
gulp.task('ava_dependencies', function () {
	gulp.src([
        'app/scripts/ava_dependencies/*.js',
        'bower_components/card/lib/js/jquery.card.js'
    ])
		.pipe(plugins.uglify())
		.pipe(plugins.concat('ava_dependencies.js'))
		.pipe(gulp.dest('dist/scripts/'))
});

gulp.task('ava_cart_widgets_templates', function () {
	// Create Js From HTML Templates.
	html2js(
    [
        {'AvaCart.PageStructure': 'app/scripts/ava_cart/page-structure/templates/default.html'},
        {'AvaCart.Widgets.logo': 'app/scripts/ava_cart/widgets/logo/templates/default.html'},
        {'AvaCart.Widgets.hotline': 'app/scripts/ava_cart/widgets/hotline/templates/default.html'},
        {'AvaCart.Widgets.cartSteps': 'app/scripts/ava_cart/widgets/cart-steps/templates/default.html'},
        {'AvaCart.Widgets.coupon': 'app/scripts/ava_cart/widgets/coupon/templates/default.html'},
//        {'AvaCart.Widgets.quantityField': 'app/scripts/ava_cart/widgets/quantity-field/templates/plus-minus.html'},
//        {'AvaCart.Widgets.quantityField': 'app/scripts/ava_cart/widgets/quantity-field/templates/minus-input-plus.html'},
//        {'AvaCart.Widgets.quantityField': 'app/scripts/ava_cart/widgets/quantity-field/templates/vertical-minus-input-plus.html'},
//        {'AvaCart.Widgets.quantityField': 'app/scripts/ava_cart/widgets/quantity-field/templates/minus-plus.html'},
//        {'AvaCart.Widgets.quantityField': 'app/scripts/ava_cart/widgets/quantity-field/templates/vertical-plus-input-minus.html'},
        {'AvaCart.Widgets.quantityField': 'app/scripts/ava_cart/widgets/quantity-field/templates/caret-caron.html'},
        {'AvaCart.Widgets.purchaseAsGift': 'app/scripts/ava_cart/widgets/purchase-as-gift/templates/default.html'},
        {'AvaCart.Widgets.downloadInsuranceService': 'app/scripts/ava_cart/widgets/download-insurance-service/templates/default.html'},
        {'AvaCart.Widgets.customBackupCDDesign': 'app/scripts/ava_cart/widgets/backup-cd/templates/default.html'},
        {'AvaCart.Widgets.crossSelling': 'app/scripts/ava_cart/widgets/cross-selling/templates/default.html'},
        {'AvaCart.Widgets.paymentOptions': 'app/scripts/ava_cart/widgets/payment-options/templates/combo.html'},
        {'AvaCart.Widgets.productsInShoppingCart': 'app/scripts/ava_cart/widgets/products-in-shopping-cart/templates/layout-3.html'},
        {'AvaCart.Widgets.whoisAvangate': 'app/scripts/ava_cart/widgets/who-is-avangate/templates/default.html'},
//        {'AvaCart.Widgets.whoisAvangate': 'app/scripts/ava_cart/widgets/who-is-avangate/templates/boxed.html'},
        {'AvaCart.Widgets.secureCheckout': 'app/scripts/ava_cart/widgets/secure-checkout/templates/table.html'},
        {'AvaCart.Widgets.fieldHelper': 'app/scripts/ava_cart/widgets/field-helper/templates/default.html'}
    ], '.tmp/ava_cart_widget_templates.js');

});

// Uglify & concatenate ava_cart scripts.
gulp.task('ava_cart', function () {
	gulp.src([
        'app/scripts/ava_cart/ava_cart.js',
        'app/scripts/ava_cart/page-structure/*.js',
        'app/scripts/ava_cart/layouts/**/*.js',
        'app/scripts/ava_cart/widgets/**/*.js'
    ])
		//.pipe(plugins.uglify())
		.on('error', plugins.util.log)
		.pipe(plugins.concat('ava_cart.js'))
		.pipe(gulp.dest('dist/scripts'))
		.pipe(plugins.notify({
			title: 'AvaCart',
			message: 'AvaCart has been compiled. Zboara puiule, zboara!'
		}));
});

// Uglify scripts.
gulp.task('scripts', ['ava_legacy', 'ava_cart', 'ava_dependencies'], function () {
	gulp.src([
        'app/scripts/*.js',
        'app/custom/scripts/*.js'
    ])
		.pipe(plugins.uglify())
		.pipe(gulp.dest('dist/scripts'));
});

// Uglify & concatenate ava_cart scripts.
gulp.task('copy', ['ava_cart_widgets_templates'], function () {
	gulp.src([
        '.tmp/ava_cart_widget_templates.js',
        'app/scripts/ava_cart/ava_cart_init.js'
    ])
		.pipe(plugins.copy('dist/scripts', {
			prefix: 3
		}));
});

// Watch files for changes & reload.
gulp.task('serve', ['styles', 'scripts', 'copy'], function (cb) {
	gulp.watch(['app/styles/**/*.{less,css}'], ['styles', reload]);
	gulp.watch(['app/scripts/**/*.js'], ['scripts']);
});

//// Build production files, the default task.
gulp.task('default', ['clean', 'jslint', /*'jscs'*/ ], function (cb) {
	runSequence('styles', ['scripts', 'copy'], cb);
});
